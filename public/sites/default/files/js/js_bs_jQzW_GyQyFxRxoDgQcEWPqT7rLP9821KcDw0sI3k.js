(function ($, Drupal, window, document) {

  'use strict';

  Drupal.behaviors.readmore = {
    attach: function (context, settings) {
      $(function () {
        $('.readmore').each(function () {
          var height = 200;
          var more = Drupal.t('More Information');
          var less = Drupal.t('Close');

          if ($(this).data('readmore-height')) {
            height = $(this).data('readmore-height');
          }

          if ($(this).data('readmore-more')) {
            more = $(this).data('readmore-more');
          }

          if ($(this).data('readmore-less')) {
            less = $(this).data('readmore-less');
          }

          $(this).readmore({
            collapsedHeight: height,
            moreLink: '<a href="#" class="more">' + more + '</a>',
            lessLink: '<a href="#" class="less">' + less + '</a>',
            speed: 300,
            blockCSS: 'display: block; width: 100%;'
          });

        });
      });
    }
  };

})(jQuery, Drupal, this, this.document);
;
(function ($, Drupal) {
  $(function () {

    // Tooltip.
    $('.info .shades a[data-tipso]').each(function () {
      $(this).tipso({
        position: 'top',
        useTitle: false,
        background: $(this).data('color-code'),
        color: $(this).data('tipso-color')
      });
    });

  });

})(jQuery, Drupal);
;
