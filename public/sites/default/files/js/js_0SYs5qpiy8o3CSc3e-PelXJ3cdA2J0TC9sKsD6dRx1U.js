Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"Hide":"\u067e\u0646\u0647\u0627\u0646 \u06a9\u0646","Show":"\u0646\u0645\u0627\u06cc\u0634","Show shortcuts":"\u0646\u0645\u0627\u06cc\u0634 \u0645\u06cc\u0627\u0646\u0628\u0631\u0647\u0627","Edit":"\u0648\u06cc\u0631\u0627\u06cc\u0634","Requires a title":"\u0639\u0646\u0648\u0627\u0646 \u0627\u062c\u0628\u0627\u0631\u06cc \u0627\u0633\u062a","Not published":"\u0645\u0646\u062a\u0634\u0631 \u0646\u0634\u062f\u0647","Drag to re-order":"\u0628\u0631\u0627\u06cc \u0628\u0627\u0632\u0622\u0631\u0627\u06cc\u06cc \u0628\u06a9\u0634\u06cc\u062f","Changes made in this table will not be saved until the form is submitted.":"\u062a\u063a\u06cc\u06cc\u0631\u0627\u062a \u0627\u06cc\u062c\u0627\u062f \u0634\u062f\u0647 \u062f\u0631 \u0627\u06cc\u0646 \u062c\u062f\u0648\u0644 \u062a\u0627 \u0632\u0645\u0627\u0646\u06cc\u06a9\u0647 \u0641\u0631\u0645 \u0627\u0631\u0633\u0627\u0644 \u0646\u06af\u0631\u062f\u062f \u0630\u062e\u06cc\u0631\u0647 \u0646\u062e\u0648\u0627\u0647\u0646\u062f \u0634\u062f.","Enabled":"\u0641\u0639\u0627\u0644","Disabled":"\u063a\u06cc\u0631\u0641\u0639\u0627\u0644","@count min":"@count \u062f\u0642\u06cc\u0642\u0647","@count sec":"@count \u062b\u0627\u0646\u06cc\u0647","Cancel":"\u0644\u063a\u0648","Tags":"\u0628\u0631\u0686\u0633\u0628\u200c\u0647\u0627","Categories":"\u062f\u0633\u062a\u0647\u200c\u0647\u0627","None":"\u0647\u06cc\u0686","Save":"\u0630\u062e\u06cc\u0631\u0647","Add":"\u0627\u0641\u0632\u0648\u062f\u0646","Keywords":"\u06a9\u0644\u0645\u0627\u062a \u06a9\u0644\u06cc\u062f\u06cc","Upload":"\u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc","Done":"\u0627\u0646\u062c\u0627\u0645","Year":"\u0633\u0627\u0644","1 hour":"1 \u0633\u0627\u0639\u062a","@count hours":"@count \u0633\u0627\u0639\u062a","1 day":"1 \u0631\u0648\u0632","@count days":"@count \u0631\u0648\u0632","Month":"\u0645\u0627\u0647","Day":"\u0631\u0648\u0632","Users":"\u06a9\u0627\u0631\u0628\u0631\u0627\u0646","Select all rows in this table":"\u0627\u0646\u062a\u062e\u0627\u0628 \u0647\u0645\u0647 \u0633\u0637\u0631\u0647\u0627 \u062f\u0631 \u0627\u06cc\u0646 \u062c\u062f\u0648\u0644","Deselect all rows in this table":"\u0639\u062f\u0645 \u0627\u0646\u062a\u062e\u0627\u0628 \u0647\u0645\u0647 \u0633\u0637\u0631\u0647\u0627 \u062f\u0631 \u0627\u06cc\u0646 \u062c\u062f\u0648\u0644","Please wait...":"\u0644\u0637\u0641\u0627 \u0635\u0628\u0631 \u06a9\u0646\u06cc\u062f...","Loading":"\u062f\u0631 \u062d\u0627\u0644 \u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc","1 year":"1 \u0633\u0627\u0644","@count years":"@count \u0633\u0627\u0644","1 week":"1 \u0647\u0641\u062a\u0647","@count weeks":"@count \u0647\u0641\u062a\u0647","1 min":"1 \u062f\u0642\u06cc\u0642\u0647","1 sec":"1 \u062b\u0627\u0646\u06cc\u0647","1 month":"1 \u0645\u0627\u0647","@count months":"@count \u0645\u0627\u0647","Only files with the following extensions are allowed: %files-allowed.":"\u0641\u0642\u0637 \u0641\u0627\u06cc\u0644\u200c\u0647\u0627\u06cc \u0628\u0627 \u067e\u0633\u0648\u0646\u062f\u0647\u0627\u06cc \u0632\u06cc\u0631 \u0645\u062c\u0627\u0632 \u0647\u0633\u062a\u0646\u062f: %files-allowed.","By @name on @date":"\u062a\u0648\u0633\u0637 @name \u062f\u0631 @date","By @name":"\u062a\u0648\u0633\u0637 @name","Not in menu":"\u062f\u0631 \u0645\u0646\u0648 \u0646\u06cc\u0633\u062a","Alias: @alias":"\u0646\u0627\u0645 \u0645\u0633\u062a\u0639\u0627\u0631: @alias","No alias":"\u0628\u062f\u0648\u0646 \u0646\u0627\u0645 \u0645\u0633\u062a\u0639\u0627\u0631","0 sec":"0 \u062b\u0627\u0646\u06cc\u0647","New revision":"\u0628\u0627\u0632\u0628\u06cc\u0646\u06cc \u062c\u062f\u06cc\u062f","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"\u062a\u0627 \u0632\u0645\u0627\u0646\u06cc\u06a9\u0647 \u0628\u0631\u0631\u0648\u06cc \u003Cem\u003E\u0630\u062e\u06cc\u0631\u0647 \u0628\u0644\u0648\u06a9\u003C\/em\u003E \u06a9\u0644\u06cc\u06a9 \u0646\u06a9\u0646\u06cc\u062f \u062a\u063a\u06cc\u06cc\u0631\u0627\u062a \u0630\u062e\u06cc\u0631\u0647 \u0646\u062e\u0648\u0627\u0647\u0646\u062f \u0634\u062f.","This permission is inherited from the authenticated user role.":"\u0627\u06cc\u0646 \u0645\u062c\u0648\u0632 \u0627\u0632 \u0646\u0642\u0634 \u06a9\u0627\u0631\u0628\u0631 \u0634\u0646\u0627\u062e\u062a\u0647 \u0634\u062f\u0647 \u06af\u0631\u0641\u062a\u0647 \u0634\u062f\u0647 \u0627\u0633\u062a.","No revision":"\u0628\u062f\u0648\u0646 \u0628\u0627\u0632\u0628\u06cc\u0646\u06cc","@number comments per page":"@number \u062f\u06cc\u062f\u06af\u0627\u0647 \u062f\u0631 \u0647\u0631 \u0635\u0641\u062d\u0647","Not restricted":"\u0645\u062d\u062f\u0648\u062f \u0646\u0634\u062f\u0647","Verify":"\u0641\u0639\u0627\u0644 \u0633\u0627\u0632\u06cc","Hours":"\u0633\u0627\u0639\u062a","Minutes":"\u062f\u0642\u06cc\u0642\u0647","Seconds":"\u062b\u0627\u0646\u06cc\u0647","Days":"\u0631\u0648\u0632","Week":"\u0647\u0641\u062a\u0647","Hour":"\u0633\u0627\u0639\u062a","Minute":"\u062f\u0642\u06cc\u0642\u0647","Second":"\u062b\u0627\u0646\u06cc\u0647","Add to Cart":"\u0627\u0641\u0632\u0648\u062f\u0646 \u0628\u0647 \u0633\u0628\u062f \u062e\u0631\u06cc\u062f","Close":"\u0628\u0633\u062a\u0646","More Information":"\u0646\u0645\u0627\u06cc\u0634 \u0628\u06cc\u0634\u062a\u0631","Compare":"\u0645\u0642\u0627\u06cc\u0633\u0647 \u06a9\u0646","Stock Notification":"\u0627\u0637\u0644\u0627\u0639 \u0631\u0633\u0627\u0646\u06cc \u0645\u0648\u062c\u0648\u062f \u0634\u062f\u0646 \u06a9\u0627\u0644\u0627","Submit":"\u0627\u0631\u0633\u0627\u0644","Customize dashboard":"\u0634\u062e\u0635\u06cc \u0633\u0627\u0632\u06cc \u062f\u0627\u0634\u0628\u0648\u0631\u062f","Select at least two products to compare":"\u0628\u0631\u0627\u06cc \u0645\u0642\u0627\u06cc\u0633\u0647 \u062d\u062f\u0627\u0642\u0644 \u062f\u0648 \u0645\u062d\u0635\u0648\u0644 \u0627\u0646\u062a\u062e\u0627\u0628 \u06a9\u0646\u06cc\u062f.","National code is invalid.":"\u06a9\u062f \u0645\u0644\u06cc \u0646\u0627 \u0645\u0639\u062a\u0628\u0631 \u0627\u0633\u062a.","Drop files here to upload":"\u0641\u0627\u06cc\u0644 \u0631\u0627 \u0628\u0631\u0627\u06cc \u0628\u0627\u0631\u06af\u0630\u0627\u0631\u06cc \u0627\u06cc\u0646\u062c\u0627 \u0631\u0647\u0627 \u06a9\u0646\u06cc\u062f","Choose image":"\u0627\u0646\u062a\u062e\u0627\u0628 \u0639\u06a9\u0633","Change image":"\u062a\u063a\u06cc\u06cc\u0631 \u0639\u06a9\u0633","@amount Toman":"@amount \u062a\u0648\u0645\u0627\u0646","Reply to @username":"\u067e\u0627\u0633\u062e \u0628\u0647 @username","\u003Cb\u003E@name\u003C\/b\u003E in \u003Cb\u003E@parent\u003C\/b\u003E group":"\u003Cb\u003E@name\u003C\/b\u003E \u062f\u0631 \u06af\u0631\u0648\u0647 \u003Cb\u003E@parent\u003C\/b\u003E","Mobile Number Or Email Address":"\u0634\u0645\u0627\u0631\u0647 \u0645\u0648\u0628\u0627\u06cc\u0644 \u06cc\u0627 \u0622\u062f\u0631\u0633 \u0627\u06cc\u0645\u06cc\u0644","Verification Code":"\u06a9\u062f \u062a\u0627\u06cc\u06cc\u062f","You Are Subscribed Successfully.":"\u0639\u0636\u0648\u06cc\u062a \u0634\u0645\u0627 \u0628\u0627 \u0645\u0648\u0641\u0642\u06cc\u062a \u062a\u0627\u06cc\u06cc\u062f \u0634\u062f.","Enter Verification Code in Above Field.":"\u06a9\u062f \u062a\u0627\u06cc\u06cc\u062f \u0631\u0627 \u062f\u0631 \u0641\u06cc\u0644\u062f \u0628\u0627\u0644\u0627 \u0648\u0627\u0631\u062f \u06a9\u0646\u06cc\u062f.","Back":"\u0628\u0627\u0632\u06af\u0634\u062a","Back to @name":"\u0628\u0627\u0632\u06af\u0634\u062a \u0628\u0647 @name","Your cart is empty":"\u0633\u0628\u062f \u062e\u0631\u06cc\u062f \u0634\u0645\u0627 \u062e\u0627\u0644\u06cc \u0627\u0633\u062a","Numbers":"\u0645\u0648\u0631\u062f","Your profit from this purchase":"\u0633\u0648\u062f \u0634\u0645\u0627 \u0627\u0632 \u0627\u06cc\u0646 \u062e\u0631\u06cc\u062f","Complete the order":"\u062a\u06a9\u0645\u06cc\u0644 \u062e\u0631\u06cc\u062f","Welcome to MooTanRoo. Your credentials are:":"\u0628\u0647 \u0645\u0648\u062a\u0646 \u0631\u0648 \u062e\u0648\u0634 \u0622\u0645\u062f\u06cc\u062f.\r\n\u0627\u0637\u0644\u0627\u0639\u0627\u062a \u0648\u0631\u0648\u062f \u0634\u0645\u0627:","Username: @username":"\u0646\u0627\u0645 \u06a9\u0627\u0631\u0628\u0631\u06cc: @username","Password: @password":"\u0631\u0645\u0632: @password","Dear @firstname":"@firstname \u0639\u0632\u06cc\u0632","Mootanrooi":"\u0645\u0648 \u062a\u0646 \u0631\u0648\u06cc\u06cc","Online payment (Accelerate with all credit cards)":"\u067e\u0631\u062f\u0627\u062e\u062a \u0627\u06cc\u0646\u062a\u0631\u0646\u062a\u06cc ( \u0628\u0627 \u062a\u0645\u0627\u0645\u06cc \u06a9\u0627\u0631\u062a\u200c\u0647\u0627\u06cc \u0639\u0636\u0648 \u0634\u062a\u0627\u0628)"}} };;
/*!
	Colorbox 1.6.4
	license: MIT
	http://www.jacklmoore.com/colorbox
*/
(function(t,e,i){function n(i,n,o){var r=e.createElement(i);return n&&(r.id=Z+n),o&&(r.style.cssText=o),t(r)}function o(){return i.innerHeight?i.innerHeight:t(i).height()}function r(e,i){i!==Object(i)&&(i={}),this.cache={},this.el=e,this.value=function(e){var n;return void 0===this.cache[e]&&(n=t(this.el).attr("data-cbox-"+e),void 0!==n?this.cache[e]=n:void 0!==i[e]?this.cache[e]=i[e]:void 0!==X[e]&&(this.cache[e]=X[e])),this.cache[e]},this.get=function(e){var i=this.value(e);return t.isFunction(i)?i.call(this.el,this):i}}function h(t){var e=W.length,i=(A+t)%e;return 0>i?e+i:i}function a(t,e){return Math.round((/%/.test(t)?("x"===e?E.width():o())/100:1)*parseInt(t,10))}function s(t,e){return t.get("photo")||t.get("photoRegex").test(e)}function l(t,e){return t.get("retinaUrl")&&i.devicePixelRatio>1?e.replace(t.get("photoRegex"),t.get("retinaSuffix")):e}function d(t){"contains"in x[0]&&!x[0].contains(t.target)&&t.target!==v[0]&&(t.stopPropagation(),x.focus())}function c(t){c.str!==t&&(x.add(v).removeClass(c.str).addClass(t),c.str=t)}function g(e){A=0,e&&e!==!1&&"nofollow"!==e?(W=t("."+te).filter(function(){var i=t.data(this,Y),n=new r(this,i);return n.get("rel")===e}),A=W.index(_.el),-1===A&&(W=W.add(_.el),A=W.length-1)):W=t(_.el)}function u(i){t(e).trigger(i),ae.triggerHandler(i)}function f(i){var o;if(!G){if(o=t(i).data(Y),_=new r(i,o),g(_.get("rel")),!U){U=$=!0,c(_.get("className")),x.css({visibility:"hidden",display:"block",opacity:""}),I=n(se,"LoadedContent","width:0; height:0; overflow:hidden; visibility:hidden"),b.css({width:"",height:""}).append(I),j=T.height()+k.height()+b.outerHeight(!0)-b.height(),D=C.width()+H.width()+b.outerWidth(!0)-b.width(),N=I.outerHeight(!0),z=I.outerWidth(!0);var h=a(_.get("initialWidth"),"x"),s=a(_.get("initialHeight"),"y"),l=_.get("maxWidth"),f=_.get("maxHeight");_.w=Math.max((l!==!1?Math.min(h,a(l,"x")):h)-z-D,0),_.h=Math.max((f!==!1?Math.min(s,a(f,"y")):s)-N-j,0),I.css({width:"",height:_.h}),J.position(),u(ee),_.get("onOpen"),O.add(F).hide(),x.focus(),_.get("trapFocus")&&e.addEventListener&&(e.addEventListener("focus",d,!0),ae.one(re,function(){e.removeEventListener("focus",d,!0)})),_.get("returnFocus")&&ae.one(re,function(){t(_.el).focus()})}var p=parseFloat(_.get("opacity"));v.css({opacity:p===p?p:"",cursor:_.get("overlayClose")?"pointer":"",visibility:"visible"}).show(),_.get("closeButton")?B.html(_.get("close")).appendTo(b):B.appendTo("<div/>"),w()}}function p(){x||(V=!1,E=t(i),x=n(se).attr({id:Y,"class":t.support.opacity===!1?Z+"IE":"",role:"dialog",tabindex:"-1"}).hide(),v=n(se,"Overlay").hide(),L=t([n(se,"LoadingOverlay")[0],n(se,"LoadingGraphic")[0]]),y=n(se,"Wrapper"),b=n(se,"Content").append(F=n(se,"Title"),R=n(se,"Current"),P=t('<button type="button"/>').attr({id:Z+"Previous"}),K=t('<button type="button"/>').attr({id:Z+"Next"}),S=t('<button type="button"/>').attr({id:Z+"Slideshow"}),L),B=t('<button type="button"/>').attr({id:Z+"Close"}),y.append(n(se).append(n(se,"TopLeft"),T=n(se,"TopCenter"),n(se,"TopRight")),n(se,!1,"clear:left").append(C=n(se,"MiddleLeft"),b,H=n(se,"MiddleRight")),n(se,!1,"clear:left").append(n(se,"BottomLeft"),k=n(se,"BottomCenter"),n(se,"BottomRight"))).find("div div").css({"float":"left"}),M=n(se,!1,"position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;"),O=K.add(P).add(R).add(S)),e.body&&!x.parent().length&&t(e.body).append(v,x.append(y,M))}function m(){function i(t){t.which>1||t.shiftKey||t.altKey||t.metaKey||t.ctrlKey||(t.preventDefault(),f(this))}return x?(V||(V=!0,K.click(function(){J.next()}),P.click(function(){J.prev()}),B.click(function(){J.close()}),v.click(function(){_.get("overlayClose")&&J.close()}),t(e).bind("keydown."+Z,function(t){var e=t.keyCode;U&&_.get("escKey")&&27===e&&(t.preventDefault(),J.close()),U&&_.get("arrowKey")&&W[1]&&!t.altKey&&(37===e?(t.preventDefault(),P.click()):39===e&&(t.preventDefault(),K.click()))}),t.isFunction(t.fn.on)?t(e).on("click."+Z,"."+te,i):t("."+te).live("click."+Z,i)),!0):!1}function w(){var e,o,r,h=J.prep,d=++le;if($=!0,q=!1,u(he),u(ie),_.get("onLoad"),_.h=_.get("height")?a(_.get("height"),"y")-N-j:_.get("innerHeight")&&a(_.get("innerHeight"),"y"),_.w=_.get("width")?a(_.get("width"),"x")-z-D:_.get("innerWidth")&&a(_.get("innerWidth"),"x"),_.mw=_.w,_.mh=_.h,_.get("maxWidth")&&(_.mw=a(_.get("maxWidth"),"x")-z-D,_.mw=_.w&&_.w<_.mw?_.w:_.mw),_.get("maxHeight")&&(_.mh=a(_.get("maxHeight"),"y")-N-j,_.mh=_.h&&_.h<_.mh?_.h:_.mh),e=_.get("href"),Q=setTimeout(function(){L.show()},100),_.get("inline")){var c=t(e).eq(0);r=t("<div>").hide().insertBefore(c),ae.one(he,function(){r.replaceWith(c)}),h(c)}else _.get("iframe")?h(" "):_.get("html")?h(_.get("html")):s(_,e)?(e=l(_,e),q=_.get("createImg"),t(q).addClass(Z+"Photo").bind("error."+Z,function(){h(n(se,"Error").html(_.get("imgError")))}).one("load",function(){d===le&&setTimeout(function(){var e;_.get("retinaImage")&&i.devicePixelRatio>1&&(q.height=q.height/i.devicePixelRatio,q.width=q.width/i.devicePixelRatio),_.get("scalePhotos")&&(o=function(){q.height-=q.height*e,q.width-=q.width*e},_.mw&&q.width>_.mw&&(e=(q.width-_.mw)/q.width,o()),_.mh&&q.height>_.mh&&(e=(q.height-_.mh)/q.height,o())),_.h&&(q.style.marginTop=Math.max(_.mh-q.height,0)/2+"px"),W[1]&&(_.get("loop")||W[A+1])&&(q.style.cursor="pointer",t(q).bind("click."+Z,function(){J.next()})),q.style.width=q.width+"px",q.style.height=q.height+"px",h(q)},1)}),q.src=e):e&&M.load(e,_.get("data"),function(e,i){d===le&&h("error"===i?n(se,"Error").html(_.get("xhrError")):t(this).contents())})}var v,x,y,b,T,C,H,k,W,E,I,M,L,F,R,S,K,P,B,O,_,j,D,N,z,A,q,U,$,G,Q,J,V,X={html:!1,photo:!1,iframe:!1,inline:!1,transition:"elastic",speed:300,fadeOut:300,width:!1,initialWidth:"600",innerWidth:!1,maxWidth:!1,height:!1,initialHeight:"450",innerHeight:!1,maxHeight:!1,scalePhotos:!0,scrolling:!0,opacity:.9,preloading:!0,className:!1,overlayClose:!0,escKey:!0,arrowKey:!0,top:!1,bottom:!1,left:!1,right:!1,fixed:!1,data:void 0,closeButton:!0,fastIframe:!0,open:!1,reposition:!0,loop:!0,slideshow:!1,slideshowAuto:!0,slideshowSpeed:2500,slideshowStart:"start slideshow",slideshowStop:"stop slideshow",photoRegex:/\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,retinaImage:!1,retinaUrl:!1,retinaSuffix:"@2x.$1",current:"image {current} of {total}",previous:"previous",next:"next",close:"close",xhrError:"This content failed to load.",imgError:"This image failed to load.",returnFocus:!0,trapFocus:!0,onOpen:!1,onLoad:!1,onComplete:!1,onCleanup:!1,onClosed:!1,rel:function(){return this.rel},href:function(){return t(this).attr("href")},title:function(){return this.title},createImg:function(){var e=new Image,i=t(this).data("cbox-img-attrs");return"object"==typeof i&&t.each(i,function(t,i){e[t]=i}),e},createIframe:function(){var i=e.createElement("iframe"),n=t(this).data("cbox-iframe-attrs");return"object"==typeof n&&t.each(n,function(t,e){i[t]=e}),"frameBorder"in i&&(i.frameBorder=0),"allowTransparency"in i&&(i.allowTransparency="true"),i.name=(new Date).getTime(),i.allowFullscreen=!0,i}},Y="colorbox",Z="cbox",te=Z+"Element",ee=Z+"_open",ie=Z+"_load",ne=Z+"_complete",oe=Z+"_cleanup",re=Z+"_closed",he=Z+"_purge",ae=t("<a/>"),se="div",le=0,de={},ce=function(){function t(){clearTimeout(h)}function e(){(_.get("loop")||W[A+1])&&(t(),h=setTimeout(J.next,_.get("slideshowSpeed")))}function i(){S.html(_.get("slideshowStop")).unbind(s).one(s,n),ae.bind(ne,e).bind(ie,t),x.removeClass(a+"off").addClass(a+"on")}function n(){t(),ae.unbind(ne,e).unbind(ie,t),S.html(_.get("slideshowStart")).unbind(s).one(s,function(){J.next(),i()}),x.removeClass(a+"on").addClass(a+"off")}function o(){r=!1,S.hide(),t(),ae.unbind(ne,e).unbind(ie,t),x.removeClass(a+"off "+a+"on")}var r,h,a=Z+"Slideshow_",s="click."+Z;return function(){r?_.get("slideshow")||(ae.unbind(oe,o),o()):_.get("slideshow")&&W[1]&&(r=!0,ae.one(oe,o),_.get("slideshowAuto")?i():n(),S.show())}}();t[Y]||(t(p),J=t.fn[Y]=t[Y]=function(e,i){var n,o=this;return e=e||{},t.isFunction(o)&&(o=t("<a/>"),e.open=!0),o[0]?(p(),m()&&(i&&(e.onComplete=i),o.each(function(){var i=t.data(this,Y)||{};t.data(this,Y,t.extend(i,e))}).addClass(te),n=new r(o[0],e),n.get("open")&&f(o[0])),o):o},J.position=function(e,i){function n(){T[0].style.width=k[0].style.width=b[0].style.width=parseInt(x[0].style.width,10)-D+"px",b[0].style.height=C[0].style.height=H[0].style.height=parseInt(x[0].style.height,10)-j+"px"}var r,h,s,l=0,d=0,c=x.offset();if(E.unbind("resize."+Z),x.css({top:-9e4,left:-9e4}),h=E.scrollTop(),s=E.scrollLeft(),_.get("fixed")?(c.top-=h,c.left-=s,x.css({position:"fixed"})):(l=h,d=s,x.css({position:"absolute"})),d+=_.get("right")!==!1?Math.max(E.width()-_.w-z-D-a(_.get("right"),"x"),0):_.get("left")!==!1?a(_.get("left"),"x"):Math.round(Math.max(E.width()-_.w-z-D,0)/2),l+=_.get("bottom")!==!1?Math.max(o()-_.h-N-j-a(_.get("bottom"),"y"),0):_.get("top")!==!1?a(_.get("top"),"y"):Math.round(Math.max(o()-_.h-N-j,0)/2),x.css({top:c.top,left:c.left,visibility:"visible"}),y[0].style.width=y[0].style.height="9999px",r={width:_.w+z+D,height:_.h+N+j,top:l,left:d},e){var g=0;t.each(r,function(t){return r[t]!==de[t]?(g=e,void 0):void 0}),e=g}de=r,e||x.css(r),x.dequeue().animate(r,{duration:e||0,complete:function(){n(),$=!1,y[0].style.width=_.w+z+D+"px",y[0].style.height=_.h+N+j+"px",_.get("reposition")&&setTimeout(function(){E.bind("resize."+Z,J.position)},1),t.isFunction(i)&&i()},step:n})},J.resize=function(t){var e;U&&(t=t||{},t.width&&(_.w=a(t.width,"x")-z-D),t.innerWidth&&(_.w=a(t.innerWidth,"x")),I.css({width:_.w}),t.height&&(_.h=a(t.height,"y")-N-j),t.innerHeight&&(_.h=a(t.innerHeight,"y")),t.innerHeight||t.height||(e=I.scrollTop(),I.css({height:"auto"}),_.h=I.height()),I.css({height:_.h}),e&&I.scrollTop(e),J.position("none"===_.get("transition")?0:_.get("speed")))},J.prep=function(i){function o(){return _.w=_.w||I.width(),_.w=_.mw&&_.mw<_.w?_.mw:_.w,_.w}function a(){return _.h=_.h||I.height(),_.h=_.mh&&_.mh<_.h?_.mh:_.h,_.h}if(U){var d,g="none"===_.get("transition")?0:_.get("speed");I.remove(),I=n(se,"LoadedContent").append(i),I.hide().appendTo(M.show()).css({width:o(),overflow:_.get("scrolling")?"auto":"hidden"}).css({height:a()}).prependTo(b),M.hide(),t(q).css({"float":"none"}),c(_.get("className")),d=function(){function i(){t.support.opacity===!1&&x[0].style.removeAttribute("filter")}var n,o,a=W.length;U&&(o=function(){clearTimeout(Q),L.hide(),u(ne),_.get("onComplete")},F.html(_.get("title")).show(),I.show(),a>1?("string"==typeof _.get("current")&&R.html(_.get("current").replace("{current}",A+1).replace("{total}",a)).show(),K[_.get("loop")||a-1>A?"show":"hide"]().html(_.get("next")),P[_.get("loop")||A?"show":"hide"]().html(_.get("previous")),ce(),_.get("preloading")&&t.each([h(-1),h(1)],function(){var i,n=W[this],o=new r(n,t.data(n,Y)),h=o.get("href");h&&s(o,h)&&(h=l(o,h),i=e.createElement("img"),i.src=h)})):O.hide(),_.get("iframe")?(n=_.get("createIframe"),_.get("scrolling")||(n.scrolling="no"),t(n).attr({src:_.get("href"),"class":Z+"Iframe"}).one("load",o).appendTo(I),ae.one(he,function(){n.src="//about:blank"}),_.get("fastIframe")&&t(n).trigger("load")):o(),"fade"===_.get("transition")?x.fadeTo(g,1,i):i())},"fade"===_.get("transition")?x.fadeTo(g,0,function(){J.position(0,d)}):J.position(g,d)}},J.next=function(){!$&&W[1]&&(_.get("loop")||W[A+1])&&(A=h(1),f(W[A]))},J.prev=function(){!$&&W[1]&&(_.get("loop")||A)&&(A=h(-1),f(W[A]))},J.close=function(){U&&!G&&(G=!0,U=!1,u(oe),_.get("onCleanup"),E.unbind("."+Z),v.fadeTo(_.get("fadeOut")||0,0),x.stop().fadeTo(_.get("fadeOut")||0,0,function(){x.hide(),v.hide(),u(he),I.remove(),setTimeout(function(){G=!1,u(re),_.get("onClosed")},1)}))},J.remove=function(){x&&(x.stop(),t[Y].close(),x.stop(!1,!0).remove(),v.remove(),G=!1,x=null,t("."+te).removeData(Y).removeClass(te),t(e).unbind("click."+Z).unbind("keydown."+Z))},J.element=function(){return t(_.el)},J.settings=X)})(jQuery,document,window);;
/**
 * @file
 * Colorbox module init js.
 */

(function ($) {

Drupal.behaviors.initColorbox = {
  attach: function (context, settings) {
    if (!$.isFunction($.colorbox) || typeof settings.colorbox === 'undefined') {
      return;
    }

    if (settings.colorbox.mobiledetect && window.matchMedia) {
      // Disable Colorbox for small screens.
      var mq = window.matchMedia("(max-device-width: " + settings.colorbox.mobiledevicewidth + ")");
      if (mq.matches) {
        return;
      }
    }

    // Use "data-colorbox-gallery" if set otherwise use "rel".
    settings.colorbox.rel = function () {
      if ($(this).data('colorbox-gallery')) {
        return $(this).data('colorbox-gallery');
      }
      else {
        return $(this).attr('rel');
      }
    };

    $('.colorbox', context)
      .once('init-colorbox')
      .colorbox(settings.colorbox);

    $(context).bind('cbox_complete', function () {
      Drupal.attachBehaviors('#cboxLoadedContent');
    });
  }
};

})(jQuery);
;
/**
 * @file
 * Colorbox module style js.
 */

(function ($) {

Drupal.behaviors.initColorboxDefaultStyle = {
  attach: function (context, settings) {
    $(context).bind('cbox_complete', function () {
      // Only run if there is a title.
      if ($('#cboxTitle:empty', context).length == false) {
        $('#cboxLoadedContent img', context).bind('mouseover', function () {
          $('#cboxTitle', context).slideDown();
        });
        $('#cboxOverlay', context).bind('mouseover', function () {
          $('#cboxTitle', context).slideUp();
        });
      }
      else {
        $('#cboxTitle', context).hide();
      }
    });
  }
};

})(jQuery);
;
/**
 * @file
 * Colorbox module inline js.
 */

(function ($) {

Drupal.behaviors.initColorboxInline = {
  attach: function (context, settings) {
    if (!$.isFunction($.colorbox) || typeof settings.colorbox === 'undefined') {
      return;
    }

    if (settings.colorbox.mobiledetect && window.matchMedia) {
      // Disable Colorbox for small screens.
      var mq = window.matchMedia("(max-device-width: " + settings.colorbox.mobiledevicewidth + ")");
      if (mq.matches) {
        return;
      }
    }

    $.urlParam = function(name, url){
      if (name == 'fragment') {
        var results = new RegExp('(#[^&#]*)').exec(url);
      }
      else {
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
      }
      if (!results) { return ''; }
      return results[1] || '';
    };
    $('.colorbox-inline', context).once('init-colorbox-inline').colorbox({
      transition:settings.colorbox.transition,
      speed:settings.colorbox.speed,
      opacity:settings.colorbox.opacity,
      slideshow:settings.colorbox.slideshow,
      slideshowAuto:settings.colorbox.slideshowAuto,
      slideshowSpeed:settings.colorbox.slideshowSpeed,
      slideshowStart:settings.colorbox.slideshowStart,
      slideshowStop:settings.colorbox.slideshowStop,
      current:settings.colorbox.current,
      previous:settings.colorbox.previous,
      next:settings.colorbox.next,
      close:settings.colorbox.close,
      overlayClose:settings.colorbox.overlayClose,
      maxWidth:settings.colorbox.maxWidth,
      maxHeight:settings.colorbox.maxHeight,
      innerWidth:function(){
        return $.urlParam('width', $(this).attr('href'));
      },
      innerHeight:function(){
        return $.urlParam('height', $(this).attr('href'));
      },
      title:function(){
        return decodeURIComponent($.urlParam('title', $(this).attr('href')));
      },
      iframe:function(){
        return $.urlParam('iframe', $(this).attr('href'));
      },
      inline:function(){
        return $.urlParam('inline', $(this).attr('href'));
      },
      href:function(){
        return $.urlParam('fragment', $(this).attr('href'));
      }
    });
  }
};

})(jQuery);
;
(function($, Drupal, window, document) {
  $(function() {
    // Payment gateway redirect.
    $(".payment-redirect-form").submit();

    const Payment = {
      init() {
        this.bindEvents();

        this.checkStepItems();
        this.getGatewayName();
      },

      elements: {
        paymentItemInput: $(".payment-item .radio-payment-item"),
        paymentOption: $(".payment-item-trigger .radio-payment-option")
      },

      data: {
        selectedPayment: null
      },
      bindEvents() {
        $('.checkout-payment-form .form-item-payment-method .form-radios').prepend('<div class="form-item online-type"><label class="option" for="edit-payment-method-online">'+Drupal.t('Online payment (Accelerate with all credit cards)')+'</label></div>');
        $('.checkout-payment-form .form-item-payment-method .form-type-radio').each(function(){
          if($(this).children('.form-radio').val() === 'cash'){
            $(this).addClass('cash');
            if($(this).children("input:checked").length) {
              $('.form-item-payment-method .online-type').removeClass('selected');
            }
          } else {
            $(this).addClass('online');
            if($(this).children("input:checked").length) {
              $('.form-item-payment-method .online-type').addClass('selected');
            }
          }
        });

        $('.checkout-payment-form .form-item-payment-method .form-radio').on('change',function(){
          if($(this).is(':checked')){
            if($(this).val() === 'cash'){
              $('.form-item-payment-method .online-type').removeClass('selected');
            } else {
              $('.form-item-payment-method .online-type').addClass('selected');
            }
          }
        });

        $('.form-item-payment-method .online-type').on('click', function(){
          $(this).addClass('selected');
          $('.checkout-payment-form .form-type-radio').eq(0).find('.form-radio').click();
        });

        this.elements.paymentItemInput.on("change", e =>
          this.handleChangePaymentItem(e)
        ),
          this.elements.paymentOption.on("change", e =>
            this.handleChangePaymentOption(e)
          );
      },

      checkStepItems() {
        if (!$(".order-step").length) {
          $(this)
            .find(".order-steps")
            .hide();
        }
      },

      handleChangePaymentOption(e) {
        console.log('handleChangePaymentOption');

        const $el = $(e.target);

        if (
          !$el.parent().hasClass("is-selected-gateway") &&
          $el.parent().siblings(".is-selected-gateway").length
        ) {
          $el.attr("checked", true);
          $el.parent().addClass("is-selected-gateway");
          this.settingPaymentHref($el.attr("name"));

          const previousSelected = $el
            .parent()
            .siblings(".is-selected-gateway");
          previousSelected.removeClass("is-selected-gateway");
          previousSelected.find(".radio-payment-option").attr("checked", false);
        }

        
      },

      checkPaymentItemSelected(element) {
        console.log('checkPaymentItemSelected');
        const selectedPayment = (this.data.selectedPayment = element
          .find("input[type=radio]")
          .val());

        if (selectedPayment === "cash") {
          console.log('cashhh');
          this.settingPaymentHref(selectedPayment);
        } else {
          console.log('onlineee');
          this.getGatewayName();
        }
      },

      handleChangePaymentItem(e) {
        console.log('handleChangePaymentItem');
        const $el = $(e.target);
        const box = $el.closest(".payment-item");

        $(".payment-item").removeClass("is-selected");
        box.toggleClass("is-selected");
        this.checkPaymentItemSelected(box);
      },

      getGatewayName() {
        if ($(".is-selected-gateway") && $(".is-selected-gateway").length) {
          var gatewayName = $(".is-selected-gateway")
            .find("input[type=radio]")
            .attr("name");

          this.settingPaymentHref(gatewayName);
        } else {
          return false;
        }
      },

      settingPaymentHref(name) {
        let currentHref = $(".finish-payment a").attr("href");
        const value = currentHref.substring(currentHref.lastIndexOf("/") + 1);

        currentHref = currentHref.replace(value, `${name}`);
        $(".finish-payment a").attr("href", currentHref);
      }
    };

    Payment.init();
  });
})(jQuery, Drupal, this, this.document);
;
/**
 * @file better_exposed_filters.js
 *
 * Provides some client-side functionality for the Better Exposed Filters module
 */
(function ($) {
  Drupal.behaviors.betterExposedFilters = {
    attach: function(context) {
      // Add highlight class to checked checkboxes for better theming
      $('.bef-tree input[type=checkbox], .bef-checkboxes input[type=checkbox]')
        // Highlight newly selected checkboxes
        .change(function() {
          _bef_highlight(this, context);
        })
        .filter(':checked').closest('.form-item', context).addClass('highlight')
      ;
    }
  };

  Drupal.behaviors.betterExposedFiltersSelectAllNone = {
    attach: function(context) {

      /*
       * Add Select all/none links to specified checkboxes
       */
      var selected = $('.form-checkboxes.bef-select-all-none:not(.bef-processed)');
      if (selected.length) {
        var selAll = Drupal.t('Select All');
        var selNone = Drupal.t('Select None');

        // Set up a prototype link and event handlers
        var link = $('<a class="bef-toggle" href="#">'+ selAll +'</a>')
        link.click(function(event) {
          // Don't actually follow the link...
          event.preventDefault();
          event.stopPropagation();

          if (selAll == $(this).text()) {
            // Select all the checkboxes
            $(this)
              .html(selNone)
              .siblings('.bef-checkboxes, .bef-tree')
                .find('.form-item input:checkbox').each(function() {
                  $(this).attr('checked', true);
                  _bef_highlight(this, context);
                })
              .end()

              // attr() doesn't trigger a change event, so we do it ourselves. But just on
              // one checkbox otherwise we have many spinning cursors
              .find('input[type=checkbox]:first').change()
            ;
          }
          else {
            // Unselect all the checkboxes
            $(this)
              .html(selAll)
              .siblings('.bef-checkboxes, .bef-tree')
                .find('.form-item input:checkbox').each(function() {
                  $(this).attr('checked', false);
                  _bef_highlight(this, context);
                })
              .end()

              // attr() doesn't trigger a change event, so we do it ourselves. But just on
              // one checkbox otherwise we have many spinning cursors
              .find('input[type=checkbox]:first').change()
            ;
          }
        });

        // Add link to the page for each set of checkboxes.
        selected
          .addClass('bef-processed')
          .each(function(index) {
            // Clone the link prototype and insert into the DOM
            var newLink = link.clone(true);

            newLink.insertBefore($('.bef-checkboxes, .bef-tree', this));

            // If all checkboxes are already checked by default then switch to Select None
            if ($('input:checkbox:checked', this).length == $('input:checkbox', this).length) {
              newLink.click();
            }
          })
        ;
      }

      // Check for and initialize datepickers
      var befSettings = Drupal.settings.better_exposed_filters;
      if (befSettings && befSettings.datepicker && befSettings.datepicker_options && $.fn.datepicker) {
        var opt = [];
        $.each(befSettings.datepicker_options, function(key, val) {
          if (key && val) {
            opt[key] = JSON.parse(val);
          }
        });
        $('.bef-datepicker').datepicker(opt);
      }

    }                   // attach: function() {
  };                    // Drupal.behaviors.better_exposed_filters = {

  Drupal.behaviors.betterExposedFiltersAllNoneNested = {
    attach:function (context, settings) {
      $('.form-checkboxes.bef-select-all-none-nested li').has('ul').once('bef-all-none-nested', function () {
        $(this)
          // To respect term depth, check/uncheck child term checkboxes.
          .find('input.form-checkboxes:first')
          .click(function() {
            var checkedParent = $(this).attr('checked');
            if (!checkedParent) {
              // Uncheck all children if parent is unchecked.
              $(this).parents('li:first').find('ul input.form-checkboxes').removeAttr('checked');
            }
            else {
              // Check all children if parent is checked.
              $(this).parents('li:first').find('ul input.form-checkboxes').attr('checked', $(this).attr('checked'));
            }
          })
          .end()
          // When a child term is checked or unchecked, set the parent term's
          // status.
          .find('ul input.form-checkboxes')
          .click(function() {
            var checked = $(this).attr('checked');

            // Determine the number of unchecked sibling checkboxes.
            var ct = $(this).parents('ul:first').find('input.form-checkboxes:not(:checked)').size();

            // If the child term is unchecked, uncheck the parent.
            if (!checked) {
              // Uncheck parent if any of the childres is unchecked.
              $(this).parents('li:first').parents('li:first').find('input.form-checkboxes:first').removeAttr('checked');
            }

            // If all sibling terms are checked, check the parent.
            if (!ct) {
              // Check the parent if all the children are checked.
              $(this).parents('li:first').parents('li:first').find('input.form-checkboxes:first').attr('checked', checked);
            }
          });
      });
    }
  };

  Drupal.behaviors.better_exposed_filters_slider = {
    attach: function(context, settings) {
      var befSettings = settings.better_exposed_filters;
      if (befSettings && befSettings.slider && befSettings.slider_options) {
        $.each(befSettings.slider_options, function(i, sliderOptions) {
          var containing_parent = "#" + sliderOptions.viewId + " #edit-" + sliderOptions.id + "-wrapper .views-widget";
          var $filter = $(containing_parent);

          // If the filter is placed in a secondary fieldset, we may not have
          // the usual wrapper element.
          if (!$filter.length) {
            containing_parent = "#" + sliderOptions.viewId + " .bef-slider-wrapper";
            $filter = $(containing_parent);
          }

          // Only make one slider per filter.
          $filter.once('slider-filter', function() {
            var $input = $(this).find('input[type=text]');

            // This is a "between" or "not between" filter with two values.
            if ($input.length == 2) {
              var $min = $input.parent().find('input#edit-' + sliderOptions.id + '-min'),
                  $max = $input.parent().find('input#edit-' + sliderOptions.id + '-max'),
                  default_min,
                  default_max;

              if (!$min.length || !$max.length) {
                return;
              }

              // Get the default values.
              // We use slider min & max if there are no defaults.
              default_min = parseFloat(($min.val() == '') ? sliderOptions.min : $min.val(), 10);
              default_max = parseFloat(($max.val() == '') ? sliderOptions.max : $max.val(), 10);
              // Set the element value in case we are using the slider min & max.
              $min.val(default_min);
              $max.val(default_max);

              $min.parents(containing_parent).after(
                $('<div class="bef-slider"></div>').slider({
                  range: true,
                  min: parseFloat(sliderOptions.min, 10),
                  max: parseFloat(sliderOptions.max, 10),
                  step: parseFloat(sliderOptions.step, 10),
                  animate: sliderOptions.animate ? sliderOptions.animate : false,
                  orientation: sliderOptions.orientation,
                  values: [default_min, default_max],
                  // Update the textfields as the sliders are moved
                  slide: function (event, ui) {
                    $min.val(ui.values[0]);
                    $max.val(ui.values[1]);
                  },
                  // This fires when the value is set programmatically or the
                  // stop event fires.
                  // This takes care of the case that a user enters a value
                  // into the text field that is not a valid step of the slider.
                  // In that case the slider will go to the nearest step and
                  // this change event will update the text area.
                  change: function (event, ui) {
                    $min.val(ui.values[0]);
                    $max.val(ui.values[1]);
                  },
                  // Attach stop listeners.
                  stop: function(event, ui) {
                    // Click the auto submit button.
                    $(this).parents('form').find('.ctools-auto-submit-click').click();
                  }
                })
              );

              // Update the slider when the fields are updated.
              $min.blur(function() {
                befUpdateSlider($(this), 0, sliderOptions);
              });
              $max.blur(function() {
                befUpdateSlider($(this), 1, sliderOptions);
              });
            }
            // This is single value filter.
            else if ($input.length == 1) {
              if ($input.attr('id') != 'edit-' + sliderOptions.id) {
                return;
              }

              // Get the default value. We use slider min if there is no default.
              var default_value = parseFloat(($input.val() == '') ? sliderOptions.min : $input.val(), 10);
              // Set the element value in case we are using the slider min.
              $input.val(default_value);

              $input.parents(containing_parent).after(
                $('<div class="bef-slider"></div>').slider({
                  min: parseFloat(sliderOptions.min, 10),
                  max: parseFloat(sliderOptions.max, 10),
                  step: parseFloat(sliderOptions.step, 10),
                  animate: sliderOptions.animate ? sliderOptions.animate : false,
                  orientation: sliderOptions.orientation,
                  value: default_value,
                  // Update the textfields as the sliders are moved.
                  slide: function (event, ui) {
                    $input.val(ui.value);
                  },
                  // This fires when the value is set programmatically or the
                  // stop event fires.
                  // This takes care of the case that a user enters a value
                  // into the text field that is not a valid step of the slider.
                  // In that case the slider will go to the nearest step and
                  // this change event will update the text area.
                  change: function (event, ui) {
                    $input.val(ui.value);
                  },
                  // Attach stop listeners.
                  stop: function(event, ui) {
                    // Click the auto submit button.
                    $(this).parents('form').find('.ctools-auto-submit-click').click();
                  }
                })
              );

              // Update the slider when the field is updated.
              $input.blur(function() {
                befUpdateSlider($(this), null, sliderOptions);
              });
            }
            else {
              return;
            }
          })
        });
      }
    }
  };

  // This is only needed to provide ajax functionality
  Drupal.behaviors.better_exposed_filters_select_as_links = {
    attach: function(context, settings) {

      $('.bef-select-as-links', context).once(function() {
        var $element = $(this);

        // Check if ajax submission is enabled. If it's not enabled then we
        // don't need to attach our custom submission handling, because the
        // links are already properly built.

        // First check if any ajax views are contained in the current page.
        if (typeof settings.views == 'undefined' || typeof settings.views.ajaxViews == 'undefined') {
          return;
        }

        // Now check that the view for which the current filter block is used,
        // is part of the configured ajax views.
        var $uses_ajax = false;
        $.each(settings.views.ajaxViews, function(i, item) {
          var $view_name = item.view_name.replace(/_/g, '-');
          var $view_display_id = item.view_display_id.replace(/_/g, '-');
          var $id = 'views-exposed-form-' + $view_name + '-' + $view_display_id;
          var $form_id = $element.parents('form').attr('id');
          if ($form_id == $id) {
            $uses_ajax = true;
            return;
          }
        });

        // If no ajax is used for form submission, we quit here.
        if (!$uses_ajax) {
          return;
        }

        // Attach selection toggle and form submit on click to each link.
        $(this).find('a').click(function(event) {
          var $wrapper = $(this).parents('.bef-select-as-links');
          var $options = $wrapper.find('select option');
          // We have to prevent the page load triggered by the links.
          event.preventDefault();
          event.stopPropagation();
          // Un select if previously seleted toogle is selected.
          var link_text = $(this).text();
          removed = '';
          $($options).each(function(i) {
            if ($(this).attr('selected')) {
              if (link_text == $(this).text()) {
                removed = $(this).text();
                $(this).removeAttr('selected');
              }
            }
          });

          // Set the corresponding option inside the select element as selected.
          $selected = $options.filter(function() {
            return $(this).text() == link_text && removed != link_text;
          });
          $selected.attr('selected', 'selected');
          $wrapper.find('.bef-new-value').val($selected.val());
          $wrapper.find('.bef-new-value[value=""]').attr("disabled", "disabled");
          $(this).addClass('active');
          // Submit the form.
          $wrapper.parents('form').find('.views-submit-button *[type=submit]').click();
        });

        $('.bef-select-as-link').ready(function() {
          $('.bef-select-as-link').find('a').removeClass('active');
          $('.bef-new-value').each(function(i, val) {
            id = $(this).parent().find('select').attr('id') + '-' + $(this).val();
            $('#'+id).find('a').addClass('active');
          });
        });
      });
    }
  };

  Drupal.behaviors.betterExposedFiltersRequiredFilter = {
    attach: function(context, settings) {
      // Required checkboxes should re-check all inputs if a user un-checks
      // them all.
      $('.bef-select-as-checkboxes', context).once('bef-required-filter').ajaxComplete(function (e, xhr, s) {
        var $element = $(this);

        if (typeof settings.views == 'undefined' || typeof settings.views.ajaxViews == 'undefined') {
          return;
        }

        // Now check that the view for which the current filter block is used,
        // is part of the configured ajax views.
        var $view_name;
        var $view_display_id;
        var $uses_ajax = false;
        $.each(settings.views.ajaxViews, function(i, item) {
          $view_name = item.view_name;
          $view_display_id = item.view_display_id;
          var $id = 'views-exposed-form-' + $view_name.replace(/_/g, '-') + '-' + $view_display_id.replace(/_/g, '-');
          var $form_id = $element.parents('form').attr('id');
          if ($form_id == $id) {
            $uses_ajax = true;
            return false;
          }
        });

        //Check if we have any filters at all because of Views Selective Filter
        if($('input', this).length > 0) {
          var $filter_name = $('input', this).attr('name').slice(0, -2);
          if (Drupal.settings.better_exposed_filters.views[$view_name].displays[$view_display_id].filters[$filter_name].required && $('input:checked', this).length == 0) {
            $('input', this).prop('checked', true);
          }
        }
      });
    }
  }

  /*
   * Helper functions
   */

  /**
   * Adds/Removes the highlight class from the form-item div as appropriate
   */
  function _bef_highlight(elem, context) {
    $elem = $(elem, context);
    $elem.attr('checked')
      ? $elem.closest('.form-item', context).addClass('highlight')
      : $elem.closest('.form-item', context).removeClass('highlight');
  }

  /**
   * Update a slider when a related input element is changed.
   *
   * We don't need to check whether the new value is valid based on slider min,
   * max, and step because the slider will do that automatically and then we
   * update the textfield on the slider's change event.
   *
   * We still have to make sure that the min & max values of a range slider
   * don't pass each other though, however once this jQuery UI bug is fixed we
   * won't have to. - http://bugs.jqueryui.com/ticket/3762
   *
   * @param $el
   *   A jQuery object of the updated element.
   * @param valIndex
   *   The index of the value for a range slider or null for a non-range slider.
   * @param sliderOptions
   *   The options for the current slider.
   */
  function befUpdateSlider($el, valIndex, sliderOptions) {
    var val = parseFloat($el.val(), 10),
        currentMin = $el.parents('div.views-widget').next('.bef-slider').slider('values', 0),
        currentMax = $el.parents('div.views-widget').next('.bef-slider').slider('values', 1);
    // If we have a range slider.
    if (valIndex != null) {
      // Make sure the min is not more than the current max value.
      if (valIndex == 0 && val > currentMax) {
        val = currentMax;
      }
      // Make sure the max is not more than the current max value.
      if (valIndex == 1 && val < currentMin) {
        val = currentMin;
      }
      // If the number is invalid, go back to the last value.
      if (isNaN(val)) {
        val = $el.parents('div.views-widget').next('.bef-slider').slider('values', valIndex);
      }
    }
    else {
      // If the number is invalid, go back to the last value.
      if (isNaN(val)) {
        val = $el.parents('div.views-widget').next('.bef-slider').slider('value');
      }
    }
    // Make sure we are a number again.
    val = parseFloat(val, 10);
    // Set the slider to the new value.
    // The slider's change event will then update the textfield again so that
    // they both have the same value.
    if (valIndex != null) {
      $el.parents('div.views-widget').next('.bef-slider').slider('values', valIndex, val);
    }
    else {
      $el.parents('div.views-widget').next('.bef-slider').slider('value', val);
    }
  }

}) (jQuery);
;
(function($){
/**
 * To make a form auto submit, all you have to do is 3 things:
 *
 * ctools_add_js('auto-submit');
 *
 * On gadgets you want to auto-submit when changed, add the ctools-auto-submit
 * class. With FAPI, add:
 * @code
 *  '#attributes' => array('class' => array('ctools-auto-submit')),
 * @endcode
 *
 * If you want to have auto-submit for every form element,
 * add the ctools-auto-submit-full-form to the form. With FAPI, add:
 * @code
 *   '#attributes' => array('class' => array('ctools-auto-submit-full-form')),
 * @endcode
 *
 * If you want to exclude a field from the ctool-auto-submit-full-form auto submission,
 * add the class ctools-auto-submit-exclude to the form element. With FAPI, add:
 * @code
 *   '#attributes' => array('class' => array('ctools-auto-submit-exclude')),
 * @endcode
 *
 * Finally, you have to identify which button you want clicked for autosubmit.
 * The behavior of this button will be honored if it's ajaxy or not:
 * @code
 *  '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click')),
 * @endcode
 *
 * Currently only 'select', 'radio', 'checkbox' and 'textfield' types are supported. We probably
 * could use additional support for HTML5 input types.
 */

Drupal.behaviors.CToolsAutoSubmit = {
  attach: function(context) {
    // 'this' references the form element
    function triggerSubmit (e) {
      if ($.contains(document.body, this)) {
        var $this = $(this);
        if (!$this.hasClass('ctools-ajaxing')) {
          $this.find('.ctools-auto-submit-click').click();
        }
      }
    }

    // the change event bubbles so we only need to bind it to the outer form
    $('form.ctools-auto-submit-full-form', context)
      .add('.ctools-auto-submit', context)
      .filter('form, select, input:not(:text, :submit)')
      .once('ctools-auto-submit')
      .change(function (e) {
        // don't trigger on text change for full-form
        if ($(e.target).is(':not(:text, :submit, .ctools-auto-submit-exclude)')) {
          triggerSubmit.call(e.target.form);
        }
      });

    // e.keyCode: key
    var discardKeyCode = [
      16, // shift
      17, // ctrl
      18, // alt
      20, // caps lock
      33, // page up
      34, // page down
      35, // end
      36, // home
      37, // left arrow
      38, // up arrow
      39, // right arrow
      40, // down arrow
       9, // tab
      13, // enter
      27  // esc
    ];
    // Don't wait for change event on textfields
    $('.ctools-auto-submit-full-form input:text, input:text.ctools-auto-submit', context)
      .filter(':not(.ctools-auto-submit-exclude)')
      .once('ctools-auto-submit', function () {
        // each textinput element has his own timeout
        var timeoutID = 0;
        $(this)
          .bind('keydown keyup', function (e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID && clearTimeout(timeoutID);
            }
          })
          .keyup(function(e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID = setTimeout($.proxy(triggerSubmit, this.form), 500);
            }
          })
          .bind('change', function (e) {
            if ($.inArray(e.keyCode, discardKeyCode) === -1) {
              timeoutID = setTimeout($.proxy(triggerSubmit, this.form), 500);
            }
          });
      });
  }
}
})(jQuery);
;
"use strict";function ownKeys(t,e){var a=Object.keys(t);if(Object.getOwnPropertySymbols){var n=Object.getOwnPropertySymbols(t);e&&(n=n.filter(function(e){return Object.getOwnPropertyDescriptor(t,e).enumerable})),a.push.apply(a,n)}return a}function _objectSpread(t){for(var e=1;e<arguments.length;e++){var a=null!=arguments[e]?arguments[e]:{};e%2?ownKeys(Object(a),!0).forEach(function(e){_defineProperty(t,e,a[e])}):Object.getOwnPropertyDescriptors?Object.defineProperties(t,Object.getOwnPropertyDescriptors(a)):ownKeys(Object(a)).forEach(function(e){Object.defineProperty(t,e,Object.getOwnPropertyDescriptor(a,e))})}return t}function _defineProperty(e,t,a){return t in e?Object.defineProperty(e,t,{value:a,enumerable:!0,configurable:!0,writable:!0}):e[t]=a,e}!function(h,s){h(function(){({init:function(){this.setPopupModalConfig(),this.thirdParties.mobileDetect=MobileDetect?new MobileDetect(window.navigator.userAgent):null,this.isMobile=this.thirdParties.mobileDetect.mobile(),this.isMobile&&this.doMobileSpecificTasks(),this.isInCartPage=h("body").hasClass("page-cart"),this.bindEvents()},reset:function(){window.clearInterval(this.data.loading.timerId),this.ajaxRequests.sendRemoveItemRequest=null,this.ajaxRequests.getCartItemsList=null,this.data.loading.waitIsOver=!1,this.data.loading.isLoading=!1,this.data.errorLoadingList=!1,this.data.loading.timerId=null},setPopupModalConfig:function(){h.fn.popup.defaults.autozindex=!0,h.fn.popup.defaults.absolute=!0,h.fn.popup.defaults.blur=!0},doMobileSpecificTasks:function(){this.elements.cartPreviewContainer.addClass("is-mobile");var e=this.thirdParties.mobileDetect._cache.phone;h("body").addClass("is-".concat(e))},thirdParties:{mobileDetect:null},elements:{cartToggler:h(".cart-toggler"),cartPreviewContainer:h(".cart-preview-container"),cartPreview:h(".cart-preview-container .cart-preview"),cartTogglerCountIndicator:h(".cart-toggler-count"),cartPreviewTemplate:h("#cart-preview-template").html()},ajaxRequests:{getCartItemsList:null,sendRemoveItemRequest:null,getUntilFreeShipping:null},data:{errorLoadingList:!1,untilFreeShipping:null,isFreeShipping:!0,loading:{waitIsOver:!1,isLoading:!1,timerId:null},cartSummary:null,isLoading:null,showList:null,list:null},bindEvents:function(){var a=this;this.setPreviewContainerCloseHandler(),this.isInCartPage||(this.elements.cartToggler.on("click",function(e){e.preventDefault(),e.stopPropagation(),a.cartTogglerClickHandler()}),this.elements.cartPreviewContainer.on("setContent",function(e,t){return a.setPreviewPaneContent(t)}),h(document).on("click",".cart-preview-item-remove",function(e){e.preventDefault(),e.target.classList.contains("fal")?h(e.target.parentNode).addClass("is-processing"):h(e.target).addClass("is-processing"),a.sendRemoveItemRequest(e.target.classList.contains("fal")?e.target.parentNode:e.target).then(function(){return a.getUntilFreeShipping()}).then(function(){a.setPreviewPaneContent({isLoading:!1}),a.updateCartCount()},function(e){a.data.errorLoadingList=!0,a.data.loading.isLoading=!1,a.data.errorStatus=e,a.setPreviewPaneContent(e)})}))},cancelPatiencyMonitor:function(){var e=this.data.loading;return window.clearInterval(e.timerId),e.waitIsOver=!1,e.timerId=null,this},monitorLoadingForPatiencyMessage:function(){var e=this,t=e.data.loading;return t.timerId=window.setInterval(function(){t.waitIsOver=t.isLoading,e.ajaxRequests.getCartItemsList&&e.setPreviewPaneContent({}),t.isLoading||e.cancelPatiencyMonitor()},8e3),h.Deferred().resolve()},sendRemoveItemRequest:function(e){var t=e.dataset,a=t.nid,n=t.iid,i=this,r=h.Deferred();return i.ajaxRequests.sendRemoveItemRequest=h.ajax({dataType:"json",url:s.settings.basePath+"api/v1/cart/remove",data:{time:(new Date).getTime(),api_key:"public",nid:a,iid:n}}),i.ajaxRequests.sendRemoveItemRequest.then(function(e){i.data.list=i.populateList(e.data.cart_items),i.data.cartSummary=e.data.cart_summary,i.ajaxRequests.sendRemoveItemRequest=null,r.resolve()},function(){i.ajaxRequests.sendRemoveItemRequest=null,r.reject()}),r},cartTogglerClickHandler:function(){var t=this,e=t.isOpen();return!t.data.loading.isLoading&&(e||t.setPreviewPaneContent({isLoading:!0}),t.openCartPreviewContainer(),!e&&void t.cancelPatiencyMonitor().monitorLoadingForPatiencyMessage().then(function(){return t.getUntilFreeShipping()}).then(function(){return t.getCartItemsList()}).then(function(e){t.setPreviewPaneContent({isLoading:!1,cartSummary:e.data.cart_summary})}).then(function(){return t.updateCartCount()}).then(function(){return t.cancelPatiencyMonitor()}))},openCartPreviewContainer:function(){var e=this;this.isMobile?(this.elements.cartPreviewContainer.popup({onopen:function(){e.cancelPatiencyMonitor()},onclose:function(){(e.ajaxRequests.getCartItemsList||e.ajaxRequests.sendRemoveItemRequest)&&(e.ajaxRequests.getCartItemsList&&e.ajaxRequests.getCartItemsList.abort&&e.ajaxRequests.getCartItemsList.abort(),e.ajaxRequests.sendRemoveItemRequest&&e.ajaxRequests.sendRemoveItemRequest.abort()&&e.ajaxRequests.sendRemoveItemRequest.abort()),e.reset()}}),this.elements.cartPreviewContainer.popup("show",{focusdelay:400,vertical:"bottom"})):this.toggleCartPreviewContainer(!this.isOpen())},isOpen:function(){return this.elements.cartPreviewContainer.hasClass("is-open")},setPreviewPaneContent:function(e){var t=e.isLoading,a=e.list,n=e.cartSummary,i=this,r=i.elements.cartPreviewTemplate;t=i.data.loading.isLoading=void 0!==t?t:i.data.loading.isLoading;var s=i.data.loading.waitIsOver;a=i.data.list=void 0!==a?a:i.data.list,(n=i.data.cartSummary=void 0!==n?n:_objectSpread({},i.data.cartSummary))&&n.payable&&(n.payable=n.payable.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")||null);var o=n&&0<n.discount;o&&(n.discount=n.discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")||null);var l=i.data.errorLoadingList,u=i.data.errorStatus,c=i.data.progressPercent=i.data.payable/i.data.freeShippingAmount*100,d=i.data.untilFreeShipping,p=0==Number(i.data.untilFreeShipping),g={cartIsEmpty:!t&&!l&&(!a||a&&0===a.length),showList:!l&&a&&a.length&&!t,error:!!l&&{errorStatus:u},cartSummary:n,hasDiscount:o,waitIsOver:s,isLoading:t,list:a,isFreeShipping:p,untilFreeShipping:d,progressPercent:c<100?c:100},m=h(Mustache.render(r,g));i.elements.cartPreview.html(m),a&&0<a.length&&!t&&i.initScrollbar()},updateCartCount:function(){var e=this.elements.cartTogglerCountIndicator,t=this.data.cartSummary.count;e.html(t),t?e.fadeIn(100):e.fadeOut(100)},initScrollbar:function(){if(this.isMobile)return!1;this.elements.cartPreviewContainer.find(".cart-preview-items").addClass("has-scrollbar").mCustomScrollbar({axis:"y",theme:"dark-thin"})},toggleCartPreviewContainer:function(e){return e?this.elements.cartPreviewContainer.fadeIn(100).addClass("is-open"):this.elements.cartPreviewContainer.fadeOut(100).removeClass("is-open"),h.Deferred().resolve()},setPreviewContainerCloseHandler:function(){var i=this;if(this.isMobile)return!1;h(document).mouseup(function(e){var t=i.elements.cartPreviewContainer,a=h(".cart-preview-container-close-link"),n=h('[data-close-handler-exception="cart-preview"], [data-close-handler-exception="cart-preview"] *');return(t.is(e.target)||0!==t.has(e.target).length||n.is(e.target)||n.has(e.target).length)&&!a.is(e.target)||i.toggleCartPreviewContainer(!1),!1})},getCartCount:function(){var t=h.Deferred();return h.ajax({dataType:"json",url:s.settings.basePath+"api/v1/cart/count",data:{time:(new Date).getTime(),api_key:"public"}}).then(function(e){t.resolve(e.data.count)},t.reject),t},getCartItemsList:function(){var t=this,a=h.Deferred();return t.data.errorLoadingList=!1,t.ajaxRequests.getCartItemsList=h.ajax({dataType:"json",url:s.settings.basePath+"api/v1/cart/list",data:{time:(new Date).getTime(),api_key:"public"}}),t.ajaxRequests.getCartItemsList.then(function(e){t.data.list=t.populateList(e.data.cart_items),t.data.cartSummary=e.data.cart_summary,t.ajaxRequests.getCartItemsList=null,a.resolve(e)}).fail(function(e){t.data.errorLoadingList=!0,t.data.loading.isLoading=!1,t.data.errorStatus=e,t.setPreviewPaneContent(e),t.ajaxRequests.getCartItemsList=null,a.fail()}),a.promise()},getUntilFreeShipping:function(){var t=this,a=h.Deferred();return t.ajaxRequests.getUntilFreeShipping=h.ajax({dataType:"json",url:s.settings.basePath+"api/v1/shipping/until-free",data:{api_key:"public"}}),t.ajaxRequests.getUntilFreeShipping.then(function(e){t.data.untilFreeShipping=e.data.until_free_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","),t.data.payable=e.data.total,t.data.freeShippingAmount=e.data.free_shipping_amount,t.ajaxRequests.getUntilFreeShipping=null,a.resolve(e)}).fail(function(e){t.ajaxRequests.getUntilFreeShipping=null,a.fail()}),a.promise()},populateList:function(t){return Object.keys(t||{}).map(function(e){return t[e]})}}).init()})}(jQuery,Drupal);;
(function defineMustache(global,factory){if(typeof exports==="object"&&exports&&typeof exports.nodeName!=="string"){factory(exports)}else if(typeof define==="function"&&define.amd){define(["exports"],factory)}else{global.Mustache={};factory(global.Mustache)}})(this,function mustacheFactory(mustache){var objectToString=Object.prototype.toString;var isArray=Array.isArray||function isArrayPolyfill(object){return objectToString.call(object)==="[object Array]"};function isFunction(object){return typeof object==="function"}function typeStr(obj){return isArray(obj)?"array":typeof obj}function escapeRegExp(string){return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}function hasProperty(obj,propName){return obj!=null&&typeof obj==="object"&&propName in obj}var regExpTest=RegExp.prototype.test;function testRegExp(re,string){return regExpTest.call(re,string)}var nonSpaceRe=/\S/;function isWhitespace(string){return!testRegExp(nonSpaceRe,string)}var entityMap={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#x2F;","`":"&#x60;","=":"&#x3D;"};function escapeHtml(string){return String(string).replace(/[&<>"'`=\/]/g,function fromEntityMap(s){return entityMap[s]})}var whiteRe=/\s*/;var spaceRe=/\s+/;var equalsRe=/\s*=/;var curlyRe=/\s*\}/;var tagRe=/#|\^|\/|>|\{|&|=|!/;function parseTemplate(template,tags){if(!template)return[];var sections=[];var tokens=[];var spaces=[];var hasTag=false;var nonSpace=false;function stripSpace(){if(hasTag&&!nonSpace){while(spaces.length)delete tokens[spaces.pop()]}else{spaces=[]}hasTag=false;nonSpace=false}var openingTagRe,closingTagRe,closingCurlyRe;function compileTags(tagsToCompile){if(typeof tagsToCompile==="string")tagsToCompile=tagsToCompile.split(spaceRe,2);if(!isArray(tagsToCompile)||tagsToCompile.length!==2)throw new Error("Invalid tags: "+tagsToCompile);openingTagRe=new RegExp(escapeRegExp(tagsToCompile[0])+"\\s*");closingTagRe=new RegExp("\\s*"+escapeRegExp(tagsToCompile[1]));closingCurlyRe=new RegExp("\\s*"+escapeRegExp("}"+tagsToCompile[1]))}compileTags(tags||mustache.tags);var scanner=new Scanner(template);var start,type,value,chr,token,openSection;while(!scanner.eos()){start=scanner.pos;value=scanner.scanUntil(openingTagRe);if(value){for(var i=0,valueLength=value.length;i<valueLength;++i){chr=value.charAt(i);if(isWhitespace(chr)){spaces.push(tokens.length)}else{nonSpace=true}tokens.push(["text",chr,start,start+1]);start+=1;if(chr==="\n")stripSpace()}}if(!scanner.scan(openingTagRe))break;hasTag=true;type=scanner.scan(tagRe)||"name";scanner.scan(whiteRe);if(type==="="){value=scanner.scanUntil(equalsRe);scanner.scan(equalsRe);scanner.scanUntil(closingTagRe)}else if(type==="{"){value=scanner.scanUntil(closingCurlyRe);scanner.scan(curlyRe);scanner.scanUntil(closingTagRe);type="&"}else{value=scanner.scanUntil(closingTagRe)}if(!scanner.scan(closingTagRe))throw new Error("Unclosed tag at "+scanner.pos);token=[type,value,start,scanner.pos];tokens.push(token);if(type==="#"||type==="^"){sections.push(token)}else if(type==="/"){openSection=sections.pop();if(!openSection)throw new Error('Unopened section "'+value+'" at '+start);if(openSection[1]!==value)throw new Error('Unclosed section "'+openSection[1]+'" at '+start)}else if(type==="name"||type==="{"||type==="&"){nonSpace=true}else if(type==="="){compileTags(value)}}openSection=sections.pop();if(openSection)throw new Error('Unclosed section "'+openSection[1]+'" at '+scanner.pos);return nestTokens(squashTokens(tokens))}function squashTokens(tokens){var squashedTokens=[];var token,lastToken;for(var i=0,numTokens=tokens.length;i<numTokens;++i){token=tokens[i];if(token){if(token[0]==="text"&&lastToken&&lastToken[0]==="text"){lastToken[1]+=token[1];lastToken[3]=token[3]}else{squashedTokens.push(token);lastToken=token}}}return squashedTokens}function nestTokens(tokens){var nestedTokens=[];var collector=nestedTokens;var sections=[];var token,section;for(var i=0,numTokens=tokens.length;i<numTokens;++i){token=tokens[i];switch(token[0]){case"#":case"^":collector.push(token);sections.push(token);collector=token[4]=[];break;case"/":section=sections.pop();section[5]=token[2];collector=sections.length>0?sections[sections.length-1][4]:nestedTokens;break;default:collector.push(token)}}return nestedTokens}function Scanner(string){this.string=string;this.tail=string;this.pos=0}Scanner.prototype.eos=function eos(){return this.tail===""};Scanner.prototype.scan=function scan(re){var match=this.tail.match(re);if(!match||match.index!==0)return"";var string=match[0];this.tail=this.tail.substring(string.length);this.pos+=string.length;return string};Scanner.prototype.scanUntil=function scanUntil(re){var index=this.tail.search(re),match;switch(index){case-1:match=this.tail;this.tail="";break;case 0:match="";break;default:match=this.tail.substring(0,index);this.tail=this.tail.substring(index)}this.pos+=match.length;return match};function Context(view,parentContext){this.view=view;this.cache={".":this.view};this.parent=parentContext}Context.prototype.push=function push(view){return new Context(view,this)};Context.prototype.lookup=function lookup(name){var cache=this.cache;var value;if(cache.hasOwnProperty(name)){value=cache[name]}else{var context=this,names,index,lookupHit=false;while(context){if(name.indexOf(".")>0){value=context.view;names=name.split(".");index=0;while(value!=null&&index<names.length){if(index===names.length-1)lookupHit=hasProperty(value,names[index]);value=value[names[index++]]}}else{value=context.view[name];lookupHit=hasProperty(context.view,name)}if(lookupHit)break;context=context.parent}cache[name]=value}if(isFunction(value))value=value.call(this.view);return value};function Writer(){this.cache={}}Writer.prototype.clearCache=function clearCache(){this.cache={}};Writer.prototype.parse=function parse(template,tags){var cache=this.cache;var tokens=cache[template];if(tokens==null)tokens=cache[template]=parseTemplate(template,tags);return tokens};Writer.prototype.render=function render(template,view,partials){var tokens=this.parse(template);var context=view instanceof Context?view:new Context(view);return this.renderTokens(tokens,context,partials,template)};Writer.prototype.renderTokens=function renderTokens(tokens,context,partials,originalTemplate){var buffer="";var token,symbol,value;for(var i=0,numTokens=tokens.length;i<numTokens;++i){value=undefined;token=tokens[i];symbol=token[0];if(symbol==="#")value=this.renderSection(token,context,partials,originalTemplate);else if(symbol==="^")value=this.renderInverted(token,context,partials,originalTemplate);else if(symbol===">")value=this.renderPartial(token,context,partials,originalTemplate);else if(symbol==="&")value=this.unescapedValue(token,context);else if(symbol==="name")value=this.escapedValue(token,context);else if(symbol==="text")value=this.rawValue(token);if(value!==undefined)buffer+=value}return buffer};Writer.prototype.renderSection=function renderSection(token,context,partials,originalTemplate){var self=this;var buffer="";var value=context.lookup(token[1]);function subRender(template){return self.render(template,context,partials)}if(!value)return;if(isArray(value)){for(var j=0,valueLength=value.length;j<valueLength;++j){buffer+=this.renderTokens(token[4],context.push(value[j]),partials,originalTemplate)}}else if(typeof value==="object"||typeof value==="string"||typeof value==="number"){buffer+=this.renderTokens(token[4],context.push(value),partials,originalTemplate)}else if(isFunction(value)){if(typeof originalTemplate!=="string")throw new Error("Cannot use higher-order sections without the original template");value=value.call(context.view,originalTemplate.slice(token[3],token[5]),subRender);if(value!=null)buffer+=value}else{buffer+=this.renderTokens(token[4],context,partials,originalTemplate)}return buffer};Writer.prototype.renderInverted=function renderInverted(token,context,partials,originalTemplate){var value=context.lookup(token[1]);if(!value||isArray(value)&&value.length===0)return this.renderTokens(token[4],context,partials,originalTemplate)};Writer.prototype.renderPartial=function renderPartial(token,context,partials){if(!partials)return;var value=isFunction(partials)?partials(token[1]):partials[token[1]];if(value!=null)return this.renderTokens(this.parse(value),context,partials,value)};Writer.prototype.unescapedValue=function unescapedValue(token,context){var value=context.lookup(token[1]);if(value!=null)return value};Writer.prototype.escapedValue=function escapedValue(token,context){var value=context.lookup(token[1]);if(value!=null)return mustache.escape(value)};Writer.prototype.rawValue=function rawValue(token){return token[1]};mustache.name="mustache.js";mustache.version="2.2.1";mustache.tags=["{{","}}"];var defaultWriter=new Writer;mustache.clearCache=function clearCache(){return defaultWriter.clearCache()};mustache.parse=function parse(template,tags){return defaultWriter.parse(template,tags)};mustache.render=function render(template,view,partials){if(typeof template!=="string"){throw new TypeError('Invalid template! Template should be a "string" '+'but "'+typeStr(template)+'" was given as the first '+"argument for mustache#render(template, view, partials)")}return defaultWriter.render(template,view,partials)};mustache.to_html=function to_html(template,view,partials,send){var result=mustache.render(template,view,partials);if(isFunction(send)){send(result)}else{return result}};mustache.escape=escapeHtml;mustache.Scanner=Scanner;mustache.Context=Context;mustache.Writer=Writer});
;
"use strict";function ownKeys(t,e){var a=Object.keys(t);if(Object.getOwnPropertySymbols){var r=Object.getOwnPropertySymbols(t);e&&(r=r.filter(function(e){return Object.getOwnPropertyDescriptor(t,e).enumerable})),a.push.apply(a,r)}return a}function _objectSpread(t){for(var e=1;e<arguments.length;e++){var a=null!=arguments[e]?arguments[e]:{};e%2?ownKeys(Object(a),!0).forEach(function(e){_defineProperty(t,e,a[e])}):Object.getOwnPropertyDescriptors?Object.defineProperties(t,Object.getOwnPropertyDescriptors(a)):ownKeys(Object(a)).forEach(function(e){Object.defineProperty(t,e,Object.getOwnPropertyDescriptor(a,e))})}return t}function _defineProperty(e,t,a){return t in e?Object.defineProperty(e,t,{value:a,enumerable:!0,configurable:!0,writable:!0}):e[t]=a,e}!function(l,i){l(function(){({init:function(){var t=this;this.bindEvents(),this.getCartCount().then(function(e){return t.updateCartCount(e)}),this.getInventoryItemInnerHeight();var e=new MobileDetect(window.navigator.userAgent);this.isMobile=e.mobile()},elements:{popupAddedToBasket:l(".popup-added-to-basket"),addedToBasketInner:l(".added-to-basket-inner"),recommendedProductWrapper:l(".recommended-product-wrapper"),cartTogglerCountIndicator:l(".cart-toggler-count"),addToCartButton:l(".add-to-cart"),generalError:l(".general-error"),inventoryItem:l(".inventory-item"),readMore:l(".readmore"),popupTemplate:l("#popup-added-to-basket-template").html(),recommendedProductTemplate:l("#recommended-product-wrapper-template").html(),generalErrorPopupTemplate:l("#general-error-template").html()},data:{nid:null,iid:null,scrollbarInitialized:!1,untilFreeShipping:null,isFreeShipping:!0,item:{beforeAdd:null,afterAdd:null},ajaxRequests:{getCartItemList:null,addToCart:null},loading:{isLoading:!1,waitIsOver:!1,timerId:null},recommendedProduct:null},bindEvents:function(){var t=this;this.elements.addToCartButton.on("click",function(e){e.preventDefault(),t.addToCartClickHandler(e)}),l(document).on("mouseenter",".product-title span",function(e){return t.handleProductTitleLength(e)}),l(document).on("mouseleave",".product-title span",function(e){l(e.target).css({right:"0"})}),l(document).on("click",".view-recommended-product",function(e){l(".view-recommended-product-wrapper").fadeIn(300),l(e.target).hide(),t.renderRecommendedProducts(),l(".view-recommended-product-container .recommended-title").remove()})},getInventoryItemInnerHeight:function(){var e=0;1<this.elements.inventoryItem.length?(e=this.elements.inventoryItem.splice(0,2).reduce(function(e,t){return l(e).innerHeight()+l(t).innerHeight()}),this.elements.readMore.data("readmore-height",e+5)):this.elements.readMore.readmore("destroy")},handleProductTitleLength:function(e){var t=l(e.target);32<t.html().length&&t.css({right:100-"".concat(t.width())+"px"})},updateCartCount:function(e){var t=this.elements.cartTogglerCountIndicator;t.html(e),e?t.fadeIn(100):t.fadeOut(100)},getCartCount:function(){var t=l.Deferred();return l.ajax({dataType:"json",url:i.settings.basePath+"api/v1/cart/count",data:{time:(new Date).getTime(),api_key:"public"}}).then(function(e){t.resolve(e.data.count)},t.reject),t},getCartItemsList:function(){var t=l.Deferred(),r=this,n=this.isMobile;return r.data.ajaxRequests.getCartItemList=l.ajax({dataType:"json",url:i.settings.basePath+"api/v1/cart/list",data:{time:(new Date).getTime(),api_key:"public"}}),r.data.ajaxRequests.getCartItemList.then(function(e){t.resolve(r.populateList(e.data.cart_items))}).fail(function(e){var t=r.elements.generalErrorPopupTemplate,a=Mustache.render(t,{data:e,isError:!0});r.elements.generalError.html(a),l("body").addClass(n?"disable-scroll":""),r.elements.generalError.addClass(n?"mobile-popup-added-to-basket":""),r.elements.generalError.popup("show",n?{focusdelay:400,vertical:"bottom"}:{}),r.toggleProcessing(!1)}),t.promise()},addToCartClickHandler:function(e){var t=l(e.target),a=this;a.data.nid=window.parseInt(t.data("nid")||t.parent().data("nid")),a.data.iid=window.parseInt(t.data("iid")||t.parent().data("iid"))||void 0,a.toggleProcessing(!0),a.destructPatiencyMessage().cancelPatiencyMonitor().monitorLoadingForPatiencyMessage().then(function(){return a.getCartItemsList()}).then(function(e){return a.setCartItems("before",e)}).then(function(){return a.addToCart()}).then(function(){return a.getRecommendedProducts()}).then(function(){a.populateData(),a.renderRecommendedProducts(),a.initScrollbar()})},destructPatiencyMessage:function(){return iziToast.destroy(),this},cancelPatiencyMonitor:function(){var e=this.data.loading;return window.clearInterval(e.timerId),e.waitIsOver=!1,e.timerId=null,this},monitorLoadingForPatiencyMessage:function(){var e=this,t=e.data.loading,a=this.isMobile;return t.timerId=window.setInterval(function(){t.waitIsOver=t.isLoading,t.waitIsOver&&iziToast.show(_objectSpread({overlayClose:!0,backgroundColor:"#11bec4",titleColor:"white",messageColor:"white",transitionInMobile:"fadeIn",transitionOutMobile:"fadeOut",message:"انجام عملیات کمی بیشتر از حد معمول به طول انجامید.لطفا شکیبا باشید.",onOpened:function(){e.cancelPatiencyMonitor()},onClosed:function(){e.monitorLoadingForPatiencyMessage()}},a?{position:"topCenter"}:{position:"bottomLeft"}))},8e3),l.Deferred().resolve()},setCartItems:function(e,t){var a=this,r=a.data.item;e="before"===e?"beforeAdd":"afterAdd",t=void 0!==t.cart_items?t.cart_items:t;var n=a.data.iid;null===r[e]&&(r[e]={}),r[e][n]={self:t.find(function(e){return window.parseInt(e.pid)===a.data.nid&&window.parseInt(e.iid)===n})||null},r[e][n].count=r[e][n].self?window.parseInt(r[e][n].self.count):0},addToCart:function(){var r=this,e=r.data,t=e.nid,a=e.iid,n=r.isMobile;r.data.ajaxRequests.addToCart=l.ajax({dataType:"json",url:i.settings.basePath+"api/v1/cart/add",data:{time:(new Date).getTime(),api_key:"public",nid:t,iid:a||void 0}}),r.data.ajaxRequests.addToCart.then(function(e){r.toggleProcessing(),r.updatePopupBody(e),l(".cart-preview-container").trigger("setContent",{isLoading:!1,list:r.populateList(e.data.cart_items),cartSummary:e.data.cart_summary})}).fail(function(e){var t=r.elements.generalErrorPopupTemplate,a=Mustache.render(t,{data:e,isError:!0});r.elements.generalError.html(a),l("body").addClass(n?"disable-scroll":""),r.elements.generalError.addClass(n?"mobile-popup-added-to-basket":""),r.elements.generalError.popup("show",n?{focusdelay:400,vertical:"bottom"}:{}),r.toggleProcessing(!1),r.toggleProcessing()})},getRecommendedProducts:function(){var t=this,a=l.Deferred(),e=t.data.nid;return l.ajax({dataType:"json",url:i.settings.basePath+"api/node/complementary/"+e,data:{time:(new Date).getTime(),api_key:"public"}}).then(function(e){t.data.recommendedProduct=e,a.resolve(e)},a.reject),a},populateData:function(){var e=this.data.recommendedProduct,a=this.populateList(e);l.each(a,function(e,t){t.price=t.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","),0===parseInt(t.special_price)?delete t.special_price:t.special_price=t.special_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","),1<t.images.length&&(t.images.length=1),0===parseInt(t.discount_percentage)&&delete t.discount_percentage,a[e]=t})},renderRecommendedProducts:function(){var e=this.data.recommendedProduct,t=this.elements.recommendedProductTemplate,a=Mustache.render(t,{recommendedProduct:e,itemAdded:!0});(this.isMobile?l(".view-recommended-product-container"):this.elements.recommendedProductWrapper).html(a)},initScrollbar:function(){if(this.isMobile)return!1;l(".recommended-product-container").mCustomScrollbar({axis:"x",theme:"dark-thin"})},updatePopupBody:function(e){var t=this,a=t.populateList(e.data.cart_items),r=(t.data.item,t.data.iid,this.isMobile),n=t.elements.popupTemplate,i=t.data.untilFreeShipping=e.data.until_free_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","),o=e.data.cart_summary.payable/e.data.free_shipping_amount*100,d=0==Number(i)?!t.data.isFreeShipping:t.data.isFreeShipping;t.setCartItems("after",a);var s=Mustache.render(n,{data:a,itemAdded:!0,isFreeShipping:d,untilFreeShipping:i,progressPercent:o<100?o:100});t.elements.addedToBasketInner.html(s),t.updateCartCount(e.data.cart_summary.count),t.elements.popupAddedToBasket.addClass("item-added"),l("body").addClass(r?"disable-scroll":""),t.elements.popupAddedToBasket.addClass(r?"mobile-popup-added-to-basket":"").popup({onopen:function(){t.cancelPatiencyMonitor()},onclose:function(){l("body").removeClass("disable-scroll")}}).popup("show",r?{focusdelay:400,vertical:"bottom"}:{})},populateList:function(t){return Object.keys(t||{}).map(function(e){return t[e]})},toggleProcessing:function(e){var r=0<arguments.length&&void 0!==e&&e,n=this;n.elements.addToCartButton.map(function(e,t){if(l(t).addClass("is-disabled"),r?l(t).attr("disabled",!0):l(t).attr("disabled",!1).removeClass("is-disabled"),l(t).data("iid")===n.data.iid){var a=l(t).find("i");if(r)return a.addClass("fa-spin fa-sync-alt").removeClass("fa-cart-plus"),!(n.data.loading.isLoading=!0);l(t).removeClass("is-disabled"),a.addClass("fa-cart-plus").removeClass("fa-spin").removeClass("fa-sync-alt"),n.data.loading.isLoading=!1}})}}).init()})}(jQuery,Drupal);;
(function ($) {

/**
 * A progressbar object. Initialized with the given id. Must be inserted into
 * the DOM afterwards through progressBar.element.
 *
 * method is the function which will perform the HTTP request to get the
 * progress bar state. Either "GET" or "POST".
 *
 * e.g. pb = new progressBar('myProgressBar');
 *      some_element.appendChild(pb.element);
 */
Drupal.progressBar = function (id, updateCallback, method, errorCallback) {
  var pb = this;
  this.id = id;
  this.method = method || 'GET';
  this.updateCallback = updateCallback;
  this.errorCallback = errorCallback;

  // The WAI-ARIA setting aria-live="polite" will announce changes after users
  // have completed their current activity and not interrupt the screen reader.
  this.element = $('<div class="progress" aria-live="polite"></div>').attr('id', id);
  this.element.html('<div class="bar"><div class="filled"></div></div>' +
                    '<div class="percentage"></div>' +
                    '<div class="message">&nbsp;</div>');
};

/**
 * Set the percentage and status message for the progressbar.
 */
Drupal.progressBar.prototype.setProgress = function (percentage, message) {
  if (percentage >= 0 && percentage <= 100) {
    $('div.filled', this.element).css('width', percentage + '%');
    $('div.percentage', this.element).html(percentage + '%');
  }
  $('div.message', this.element).html(message);
  if (this.updateCallback) {
    this.updateCallback(percentage, message, this);
  }
};

/**
 * Start monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.startMonitoring = function (uri, delay) {
  this.delay = delay;
  this.uri = uri;
  this.sendPing();
};

/**
 * Stop monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.stopMonitoring = function () {
  clearTimeout(this.timer);
  // This allows monitoring to be stopped from within the callback.
  this.uri = null;
};

/**
 * Request progress data from server.
 */
Drupal.progressBar.prototype.sendPing = function () {
  if (this.timer) {
    clearTimeout(this.timer);
  }
  if (this.uri) {
    var pb = this;
    // When doing a post request, you need non-null data. Otherwise a
    // HTTP 411 or HTTP 406 (with Apache mod_security) error may result.
    $.ajax({
      type: this.method,
      url: this.uri,
      data: '',
      dataType: 'json',
      success: function (progress) {
        // Display errors.
        if (progress.status == 0) {
          pb.displayError(progress.data);
          return;
        }
        // Update display.
        pb.setProgress(progress.percentage, progress.message);
        // Schedule next timer.
        pb.timer = setTimeout(function () { pb.sendPing(); }, pb.delay);
      },
      error: function (xmlhttp) {
        pb.displayError(Drupal.ajaxError(xmlhttp, pb.uri));
      }
    });
  }
};

/**
 * Display errors on the page.
 */
Drupal.progressBar.prototype.displayError = function (string) {
  var error = $('<div class="messages error"></div>').html(string);
  $(this.element).before(error).hide();

  if (this.errorCallback) {
    this.errorCallback(this);
  }
};

})(jQuery);
;
(function ($, Drupal) {
  $(function () {

    // Update notification count for logged-in users.
    if ($('body').hasClass('logged-in')) {
      $.ajax({
        dataType: "json",
        url: Drupal.settings.basePath + 'api/v1/notification/count',
        data: {
          time: new Date().getTime(),
          api_key: 'public'
        },
        success: function (response) {
          var notificationNumber = response.data.count;
          var formatNotification= null;

          if (notificationNumber > 1000) {
            formatNotification = (notificationNumber/1000).toFixed(1).replace(/\.0$/, '') + 'k';
          } else {
            formatNotification= notificationNumber;
          }

          if (response.data.count) {
            $('.c7-u6-n11-c4').html(formatNotification).fadeIn();
          }
        }
      });
    }

    // Hide notification-group when click outside it.
    $(document).on('click', function (event) {
      $('.c7-u6-n11-t6, .c7-u6-p4-t6').removeClass('active');
      $('.c7-u6 .n11-g4-l3, .c7-u6 .p4-t10-l3').fadeOut(function () {
        $(this).remove();
      });
    });
  });
})(jQuery, Drupal);
;
"use strict";!function(t,o){t(function(){var a=null;t('.synonym-search-form input[name="keyword"]').on("input",function(n){clearTimeout(a),a=setTimeout(function(){return function(n){if(null===n||""===n||n.length<=1)return void t(".synonym-search-form .suggestions").hide();t.ajax({dataType:"json",url:o.settings.basePath+o.settings.pathPrefix+"api/v1/search/suggestion",data:{api_key:"public",keyword:n,count:4},success:function(n){var s,e,a=t(".synonym-search-form .suggestions").html("").show();n.data.taxonomy_terms.length&&(s='<div class="items"><h3><span>'+o.t("Categories")+"</span></h3>",t.each(n.data.taxonomy_terms,function(n,a){e=a.parent?o.t("<b>@name</b> in <b>@parent</b> group",{"@name":a.name,"@parent":a.parent.name}):a.name,s+='<a class="taxonomy-term" href="/taxonomy/term/'+a.tid+'/redirect">'+e+"</a>"}),s+="</div>",a.append(s)),n.data.synonyms.length&&(s='<div class="items"><h3><span>'+o.t("Keywords")+"</span></h3>",t.each(n.data.synonyms,function(n,a){s+='<a class="synonym" href="/search?keyword='+a.word+'">'+a.word+"</a>"}),s+="</div>",a.append(s)),n.data.accounts.length&&(s='<div class="items"><h3><span>'+o.t("Users")+"</span></h3>",t.each(n.data.accounts,function(n,a){s+='<a class="account" href="'+a.url+'"><img src="'+a.picture+'" alt="'+a.realname+'"/> '+a.realname+"</a>"}),s+="</div>",a.append(s)),t(".synonym-search-form").addClass("open")}})}(t(n.target).val().trim())},500)}),t('.synonym-search-form input[name="keyword"]').focus(function(){t(this).parents("form").addClass("focus")}),t('.synonym-search-form input[name="keyword"]').blur(function(){t(this).parents("form").removeClass("focus")}),t(document).click(function(){t(".synonym-search-form .suggestions").fadeOut(200),t(".synonym-search-form").removeClass("open")})})}(jQuery,Drupal);;
(function ($, Drupal, window, document) {

  'use strict';

  $(function () {

    // Initialize swiper when document ready.
    var slide_slides = new Swiper('.slide-slides .swiper-container', {
      effect: 'fade',
      loop: true,
      autoplay: {
        delay: 6000,
      },
      pagination: {
        el: '.slide-slides .swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          var title = $('.slide-slides .swiper-slide[data-index=' + index + '] img').attr('alt');
          return '<span class="' + className + '">' + title + '</span>';
        },
      },
      navigation: {
        nextEl: '.slide-slides .swiper-button-next',
        prevEl: '.slide-slides .swiper-button-prev',
      }
    });
    slide_slides.init();

    // Handle Slide Clicks. /slide/%slide counts clicks.
    $('.slide-slides .swiper-slide').click(function (event) {
      event.preventDefault();

      document.location = $(this).data('href');
    });

  });

})(jQuery, Drupal, this, this.document);
;
/*!
	Autosize 3.0.20
	license: MIT
	http://www.jacklmoore.com/autosize
*/
!function(e,t){if("function"==typeof define&&define.amd)define(["exports","module"],t);else if("undefined"!=typeof exports&&"undefined"!=typeof module)t(exports,module);else{var n={exports:{}};t(n.exports,n),e.autosize=n.exports}}(this,function(e,t){"use strict";function n(e){function t(){var t=window.getComputedStyle(e,null);"vertical"===t.resize?e.style.resize="none":"both"===t.resize&&(e.style.resize="horizontal"),s="content-box"===t.boxSizing?-(parseFloat(t.paddingTop)+parseFloat(t.paddingBottom)):parseFloat(t.borderTopWidth)+parseFloat(t.borderBottomWidth),isNaN(s)&&(s=0),l()}function n(t){var n=e.style.width;e.style.width="0px",e.offsetWidth,e.style.width=n,e.style.overflowY=t}function o(e){for(var t=[];e&&e.parentNode&&e.parentNode instanceof Element;)e.parentNode.scrollTop&&t.push({node:e.parentNode,scrollTop:e.parentNode.scrollTop}),e=e.parentNode;return t}function r(){var t=e.style.height,n=o(e),r=document.documentElement&&document.documentElement.scrollTop;e.style.height="auto";var i=e.scrollHeight+s;return 0===e.scrollHeight?void(e.style.height=t):(e.style.height=i+"px",u=e.clientWidth,n.forEach(function(e){e.node.scrollTop=e.scrollTop}),void(r&&(document.documentElement.scrollTop=r)))}function l(){r();var t=Math.round(parseFloat(e.style.height)),o=window.getComputedStyle(e,null),i=Math.round(parseFloat(o.height));if(i!==t?"visible"!==o.overflowY&&(n("visible"),r(),i=Math.round(parseFloat(window.getComputedStyle(e,null).height))):"hidden"!==o.overflowY&&(n("hidden"),r(),i=Math.round(parseFloat(window.getComputedStyle(e,null).height))),a!==i){a=i;var l=d("autosize:resized");try{e.dispatchEvent(l)}catch(e){}}}if(e&&e.nodeName&&"TEXTAREA"===e.nodeName&&!i.has(e)){var s=null,u=e.clientWidth,a=null,p=function(){e.clientWidth!==u&&l()},c=function(t){window.removeEventListener("resize",p,!1),e.removeEventListener("input",l,!1),e.removeEventListener("keyup",l,!1),e.removeEventListener("autosize:destroy",c,!1),e.removeEventListener("autosize:update",l,!1),Object.keys(t).forEach(function(n){e.style[n]=t[n]}),i.delete(e)}.bind(e,{height:e.style.height,resize:e.style.resize,overflowY:e.style.overflowY,overflowX:e.style.overflowX,wordWrap:e.style.wordWrap});e.addEventListener("autosize:destroy",c,!1),"onpropertychange"in e&&"oninput"in e&&e.addEventListener("keyup",l,!1),window.addEventListener("resize",p,!1),e.addEventListener("input",l,!1),e.addEventListener("autosize:update",l,!1),e.style.overflowX="hidden",e.style.wordWrap="break-word",i.set(e,{destroy:c,update:l}),t()}}function o(e){var t=i.get(e);t&&t.destroy()}function r(e){var t=i.get(e);t&&t.update()}var i="function"==typeof Map?new Map:function(){var e=[],t=[];return{has:function(t){return e.indexOf(t)>-1},get:function(n){return t[e.indexOf(n)]},set:function(n,o){e.indexOf(n)===-1&&(e.push(n),t.push(o))},delete:function(n){var o=e.indexOf(n);o>-1&&(e.splice(o,1),t.splice(o,1))}}}(),d=function(e){return new Event(e,{bubbles:!0})};try{new Event("test")}catch(e){d=function(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!1),t}}var l=null;"undefined"==typeof window||"function"!=typeof window.getComputedStyle?(l=function(e){return e},l.destroy=function(e){return e},l.update=function(e){return e}):(l=function(e,t){return e&&Array.prototype.forEach.call(e.length?e:[e],function(e){return n(e,t)}),e},l.destroy=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],o),e},l.update=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],r),e}),t.exports=l});;
(function($, Drupal, window, document) {
  "use strict";

  $(function() {
    const md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) {
      $("body").addClass("is-mobile");
    }

    // Initialize swiper when document ready.
    new Swiper(
      "body:not(.is-mobile) .wall-post-slides-social .swiper-container",
      {
        slidesPerView: 3,
        spaceBetween: 20,
        navigation: {
          nextEl: ".wall-post-slides-social .swiper-button-next",
          prevEl: ".wall-post-slides-social .swiper-button-prev"
        },
        breakpoints: {
          1111: {
            slidesPerView: 3
          },
          999: {
            slidesPerView: 2
          },
          555: {
            slidesPerView: 1
          }
        }
      }
    );

    // Initialize swiper when document ready.
    new Swiper(
      "body:not(.is-mobile) .wall-post-slides-official .swiper-container",
      {
        slidesPerView: 4,
        spaceBetween: 20,
        navigation: {
          nextEl: ".wall-post-slides-official .swiper-button-next",
          prevEl: ".wall-post-slides-official .swiper-button-prev"
        },
        breakpoints: {
          // 1111: {
          //   slidesPerView: 4,
          // },
          1111: {
            slidesPerView: 3
          },
          999: {
            slidesPerView: 2
          },
          555: {
            slidesPerView: 1
          }
        }
      }
    );

    // Initialize swiper when document ready.
    new Swiper(
      "body:not(.is-mobile) .wall-post-slides-related .swiper-container",
      {
        slidesPerView: 4,
        spaceBetween: 20,
        navigation: {
          nextEl: ".wall-post-slides-related .swiper-button-next",
          prevEl: ".wall-post-slides-related .swiper-button-prev"
        },
        breakpoints: {
          // 1111: {
          //   slidesPerView: 4,
          // },
          1111: {
            slidesPerView: 3
          },
          999: {
            slidesPerView: 2
          },
          555: {
            slidesPerView: 1
          }
        }
      }
    );
  });
})(jQuery, Drupal, this, this.document);
;
(function ($, Drupal, window, document) {
  'use strict';
  Drupal.behaviors.wall = {
    attach: function (context, settings) {

    }
  };

  $(function () {
    // Enable auto resize on textarea.
    autosize($('#wall-post-form textarea, .wall-post-comment-form textarea'));

    // Enable tagging.
    if ($('#tagging').length) {
      var $tagging = $('#tagging').tagging({
        'no-duplicate': false,
        'edit-on-delete': false
      });
      $tagging[0].tagging('removeSpecialKeys', ['add', 32]);
      $tagging[0].tagging('removeSpecialKeys', ['add', 188]);
    }

    $('.type-zone').on('input', function () {
      if ($(this).val() === '') {
        $(this).attr('placeholder', Drupal.t('Tags'));
      }

      var fake = $('<span>').hide().appendTo(document.body);
      var width = fake.text($(this).val() || $(this).attr('placeholder')).width() + 20;
      fake.remove();

      $(this).css({ width: Math.max(width, 50) });
    }).trigger('input');

    // Auto upload files.
    $('#wall-post-form').delegate('input.form-file', 'change', function () {
      $(this).next('input[type="submit"]').mousedown();
    });

    // Toggle medias wrapper.
    $('#wall-post-form').delegate('.upload', 'click', function () {
      // Show/Hide medias wrapper.
      $('#wall-post-form .medias-wrapper').toggleClass('visible');

      // Focus on form textarea.
      $('#wall-post-form textarea').focus();
    });

    // Handle reply button.
    $('.wall-post-item').delegate('.reply', 'click', function () {
      var $wall_post_comment = $(this).parent();
      var $wpcid = $wall_post_comment.data('wpcid');
      var $wpid = $wall_post_comment.data('wpid');
      var $wall_post_comment_form = $('#wall-post-' + $wpid).find('.wall-post-comment-form');
      $wall_post_comment_form.find('input[name=parent]').val($wpcid);
      $wall_post_comment_form.find('.reply-to').html('<i class="fal fa-times"></i> ' + Drupal.t('Reply to @username', { '@username': $wall_post_comment.find('> .username').text() }));
      $wall_post_comment_form.insertAfter($wall_post_comment);
    });

    // Handle reply-to button.
    $('.wall-post-item').delegate('.reply-to', 'click', function () {
      var $wall_post_comment_form = $(this).parents('form');
      $wall_post_comment_form.find('input[name=parent]').val(0);
      $wall_post_comment_form.find('.reply-to').html('');
      $wall_post_comment_form.insertAfter($(this).parents('.wall-post-comments'));
    });

    // Wall post menu
    $('.wall-post-item .menu-toggle').click(function () {
      $(this).next('.wall-post-menu').toggle();
    });

    // Initialize swiper when document ready.
    new Swiper('.wall-post-item .swiper-container', {
      effect: 'horizontal',
      autoHeight: true,
      spaceBetween: 0,
      navigation: {
        nextEl: '.wall-post-item .swiper-button-next',
        prevEl: '.wall-post-item .swiper-button-prev'
      }
    });

    // Table of Contents.
    if ($('.wall-post-view-mode-full .body-format-full_html').find('h2,h3,h4').length) {
      $('.toc ol').toc({
        content: '.wall-post-view-mode-full .body-format-full_html',
        headings: 'h2,h3,h4'
      });
    } else {
      $('.toc').remove();
    }
  });

})(jQuery, Drupal, this, this.document);
;
(function ($, Drupal, window, document) {

  'use strict';

  $(function () {
    // ...

    const md = new MobileDetect(window.navigator.userAgent);
    if (md.mobile()) {
      $('body').addClass('is-mobile');
    }

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-discounted .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-discounted .swiper-button-next',
        prevEl: '.product-slides-discounted .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5,
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-recommendation .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-recommendation .swiper-button-next',
        prevEl: '.product-slides-recommendation .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5,
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-promoted .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-promoted .swiper-button-next',
        prevEl: '.product-slides-promoted .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5,
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-most_viewed .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-most_viewed .swiper-button-next',
        prevEl: '.product-slides-most_viewed .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5,
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-most_ordered .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-most_ordered .swiper-button-next',
        prevEl: '.product-slides-most_ordered .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-latest .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-latest .swiper-button-next',
        prevEl: '.product-slides-latest .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-related .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-related .swiper-button-next',
        prevEl: '.product-slides-related .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-complementary .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-complementary .swiper-button-next',
        prevEl: '.product-slides-complementary .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5
        }
      }
    });

    // Initialize swiper when document ready.
    new Swiper('body:not(.is-mobile) .product-slides-master .swiper-container', {
      slidesPerView: 5,
      spaceBetween: 20,
      loop: true,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.product-slides-master .swiper-button-next',
        prevEl: '.product-slides-master .swiper-button-prev',
      },
      breakpoints: {
        1333: {
          slidesPerView: 4,
        },
        999: {
          slidesPerView: 3,
        },
        777: {
          slidesPerView: 2,
        },
        400: {
          slidesPerView: 1.5
        }
      }
    });


  });

})(jQuery, Drupal, this, this.document);
;
(function ($) {
  $(function () {

    // Pre-load
    $('*[data-swap]').each(function () {
      $('<img/>')[0].src = $(this).data('swap');
    });

    $('body').hoverIntent({
      over: function () {
        var src = $(this).attr('src');
        $(this).attr('src', $(this).data('swap'));
        $(this).data('swap', src);
      },
      out: function () {
        var src = $(this).attr('src');
        $(this).attr('src', $(this).data('swap'));
        $(this).data('swap', src);
      },
      selector: '*[data-swap]'
    });

  });
})(jQuery);;
