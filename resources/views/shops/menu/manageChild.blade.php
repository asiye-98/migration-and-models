<ul class="navbar-nav">
    @foreach ($menus as $item)
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            {{ $item->title  }}
        </a>
        @include('shops.menu.manageChild',['menus'=>$item->childs])
    </li>
    @endforeach
</ul>
