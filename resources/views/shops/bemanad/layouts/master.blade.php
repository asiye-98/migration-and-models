
@include('shops.bemanad.layouts.header')
@include('shops.bemanad.layouts.navbar')
  <!-- Page Content -->
  <div class="container">

    <div class="row">

@yield('content')

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

@include('shops.bemanad.layouts.footer')
