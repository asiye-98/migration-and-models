@include('shops.mactel.layouts.header')
@include('shops.mactel.layouts.navbar')
<!-- Page Content -->
<div class="container">

    <div class="row">

        @yield('content')

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

@include('shops.mactel.layouts.footer')
