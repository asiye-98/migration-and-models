@include('admin.layouts.header')

@include('admin.layouts.wrapper.main-header')
@include('admin.layouts.wrapper.main-sidebar')



<div class="content-wrapper">
@yield('content')

</div>


@include('admin.layouts.wrapper.control-sidebar')
@include('admin.layouts.wrapper.main-footer')

@include('admin.layouts.footer')
