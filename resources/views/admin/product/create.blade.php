@extends('admin.layouts.master')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">صفحه ایجاد محصول</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-left">
                    <li class="breadcrumb-item"><a href="#">خانه</a></li>
                    <li class="breadcrumb-item active">صفحه محصولات</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="card">
            <!-- /.card-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ route('product.store') }}" method="POST">
                @csrf
                <div class="card-body">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                name="title" placeholder="نام محصول" required autocomplete="title">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" class="form-control @error('price') is-invalid @enderror" name="price"
                                placeholder="قیمت" required autocomplete="price">
                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" class="form-control @error('inventory') is-invalid @enderror"
                                name="inventory" placeholder="موجودی" required autocomplete="inventory">
                            @error('inventory')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea type="text" class="form-control @error('description') is-invalid @enderror"
                                name="description" placeholder="توضیحات" rows="3" required autocomplete="description">
                            </textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">ورود</button>
                    <a href="{{ route('product.index') }}" class="btn btn-default float-left">لغو</a>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
    </div>
</div>





@endsection
