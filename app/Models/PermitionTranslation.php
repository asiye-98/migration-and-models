<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermitionTranslation extends Model
{
    protected $fillable = [
        'name','type','permition_id','lang',
    ];
    public function permition() {
        return $this->belonsTo(Permition::class);
    }
}
