<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'parent_id',
    ];
    public function categorytranslatin() {
        return $this->hasMany(CategoryTranslation::class);
    }
    public function currentTranslation() {
        $lang = app()->getLocate();
        return $this->hasOne(CategoryTranslation::class, 'category_id')->where('lang',$lang);
    }

    public function product() {
        return $this->hasMany(Product::class);
    }
}
