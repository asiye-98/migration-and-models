<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'user_id','category_id','oldprice','newprice','discount','count','view_count','sale_count',
    ];
    public function producttranslatin() {
        return $this->hasMany(ProductTranslation::class);
    }
    public function currentTranslation() {
        $lang = app()->getLocate();
        return $this->hasOne(ProductTranslation::class, 'product_id')->where('lang',$lang);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }
    public function images() {
        return $this->morphMany(Image::class,"imageable");
    }
}
