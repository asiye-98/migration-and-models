<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleTranslation extends Model
{
    protected $fillable = [
        'name','type','lang','role_id',
    ];
    public function role() {
        return $this->belonsTo(Role::class);
    }
}
