<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    
    protected $fillable = [
        'domainable_id','domainable_type','name',
    ];
    public function domainable() {
        return $this->morphTo();
    }
}
