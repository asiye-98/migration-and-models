<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function roletranslatin() {
        return $this->hasMany(RoleTranslation::class);
    }
    public function currentTranslation() {
        $lang = app()->getLocate();
        return $this->hasOne(RoleTranslation::class, 'role_id')->where('lang',$lang);
    }

    public function permitions() {
        return $this->belongsToMany(Permition::class);
    }
    public function users() {
        return $this->belongsToMany(User::class);
    }
}
