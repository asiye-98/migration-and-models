<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    protected $fillable = [
        'user_id','product_id',
    ];
    public function producertranslatin() {
        return $this->hasMany(ProducerTranslation::class);
    }
    public function currentTranslation() {
        $lang = app()->getLocate();
        return $this->hasOne(ProducerTranslation::class, 'producer_id')->where('lang',$lang);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
