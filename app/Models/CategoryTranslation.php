<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $fillable = [
        'lang','title','category_id',
    ];
    public function category() {
        return $this->belonsTo(Category::class);
    }
}
