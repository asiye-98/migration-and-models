<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $fillable = [
        'title','description','lang','product_id',
    ];
    public function product() {
        return $this->belonsTo(Product::class);
    }
}
