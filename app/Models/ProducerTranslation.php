<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProducerTranslation extends Model
{
    protected $fillable = [
        'company','lang','producer_id',
    ];
    public function producer() {
        return $this->belonsTo(Producer::class);
    }
}
