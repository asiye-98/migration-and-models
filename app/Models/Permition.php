<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permition extends Model
{
    public function permitiontranslatin() {
        return $this->hasMany(PermitionTranslation::class);
    }
    public function currentTranslation() {
        $lang = app()->getLocate();
        return $this->hasOne(PermitionTranslation::class, 'permition_id')->where('lang',$lang);
    }
    public function roles() {
        return $this->belongsToMany(Role::class);
    }
}
