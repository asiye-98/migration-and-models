<?php

namespace App\Http\View\Composers;


use App\Model\Menu;
use Illuminate\View\View;

class MenuComposser
{
    public function compose(View $view)
    {

        $view->with('menus', Menu::query()->where('parent_id','=',0)->get());
    }
}
