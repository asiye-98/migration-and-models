<?php

namespace App\Http\View\Composers;


use App\Model\Menu;
use Illuminate\View\View;

class AllMenuComposser
{
    public function compose(View $view)
    {

        $view->with('allMenus',Menu::pluck('title','id')->all());
    }
}
